<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->increments('id');

            $table->string('customer');
            $table->string('from');
            $table->string('to');
            $table->dateTime('when');
            $table->float('fees_cash')->default(0);
            $table->float('fees_credit')->default(0);
            $table->float('fare_cash')->default(0);
            $table->float('voucher_amount')->default(0);
            $table->float('tip')->default(0);
            $table->string('voucher_number')->nullable();

            $table->unsignedInteger('trip_sheet_id')->nullable();
            $table->unsignedInteger('user_id');

            $table->mediumText('image')->nullable();
           
            // $table->foreign('trip_sheet_id')->references('id')->on('trip_sheets'); //driver id

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
