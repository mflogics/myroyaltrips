<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_sheets', function (Blueprint $table) {
            $table->increments('id');
           
            $table->integer('mileage_start')->defualt(0);
            $table->integer('mileage_end')->defualt(0);
            $table->string('note',500)->nullable();
            
            $table->string('status')->nullable();
            $table->string('car')->nullable();
            $table->string('engine_oil')->nullable();
            $table->string('transmission_fluid')->nullable();
            $table->string('coolant')->nullable();
            $table->string('payment_status')->nullable();

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users'); //driver id
           

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_sheets');
    }
}
