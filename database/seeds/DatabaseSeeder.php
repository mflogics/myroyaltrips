<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [

            [
                'name' => 'Administrator1',
                'email' => 'admin@example.com',
                'role' => 'admin',
                'password' => Hash::make('adminadmin'),
                'hourly_rate' => 0,
                'currency' => 'EUR',
            ],

            [
                'name' => 'Administrator2',
                'email' => 'admin@admin.com',
                'role' => 'admin',
                'password' => Hash::make('adminadmin'),
                'hourly_rate' => 0,
                'currency' => 'EUR',
            ],
            [
                'name' => 'Driver1',
                'email' => 'user@user.com',
                'role' => 'user',
                'password' => Hash::make('useruser'),
                'hourly_rate' => 0,
                'currency' => 'EUR',
            ],


        ];

        foreach($users as $user){

            if(User::where('email', $user['email'])->first())
                {
                    continue;
                }

            User::create($user);    
        }

        // $this->call(ProjectSeeder::class);

    }
}
