<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    //
    protected $fillable = [
        'customer',
        'from',
        'to',
        'when',
        'fees_cash',
        'fees_credit',
        'fare_cash',
        'voucher_amount',
        'tip',
        'voucher_number',
        'trip_sheet_id',
        'user_id',
        'image',
    ];

    protected $dates = [
        'when',
    ];

    // protected $dateFormat = 'Y-m-d H:i A';

    public function getWhenAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(env('DATE_FORMAT'));
    }

    public function setWhenAttribute($date)
    {
        $this->attributes['when'] = Carbon::createFromFormat(env('DATE_FORMAT'), $date)->format('Y-m-d H:i:s');
    }

    public function trip_sheet()
    {
        return $this->belongsTo('App\TripSheet');
    }

    public function getGrossSaleAttribute($value)
    {
        return $this->fees_cash + $this->fees_credit + $this->fare_cash + $this->voucher_amount + $this->tip;
    }

    public static function getStats($trips)
    {
        $TOTAL_TRIPS = $trips->count();
        $GROSS_SALE = 0;
        $TOTAL_FEES = 0;
        $TOTAL_CASH_CREDIT_FARE = 0;
        $GROSS_TO_ROYAL = 0;
        $TOTAL_CREDIT_FARE_TRIPS = 0;
        $MISC_EXPENSES = 0;

        $TOTAL_CREDIT_CHARGES = 0;
        $DRIVER_OWES = 0;
        $LEASE_FEE = 0;

        foreach ($trips as $trip) {
            $GROSS_SALE += $trip->gross_sale;
            $TOTAL_FEES += $trip->fees_cash + $trip->fees_credit;
            $TOTAL_CASH_CREDIT_FARE += $trip->fare_cash + $trip->voucher_amount;
            $TOTAL_CREDIT_FARE_TRIPS += $trip->fees_credit + $trip->voucher_amount + $trip->tip;
            
        }

        // die($CREDIT);
        $SIXTY_PERCENT = $TOTAL_CASH_CREDIT_FARE * 60 / 100;
        $GROSS_TO_ROYAL = $TOTAL_FEES + $SIXTY_PERCENT + 6;
        $TOTAL_CREDIT_CHARGES = $TOTAL_CREDIT_FARE_TRIPS + $MISC_EXPENSES;
        $DRIVER_OWES = $GROSS_TO_ROYAL - $TOTAL_CREDIT_CHARGES;

        return compact(
            'GROSS_SALE',
            'TOTAL_FEES',
            'LEASE_FEE',
            'TOTAL_CASH_CREDIT_FARE',
            'TOTAL_CREDIT_FARE_TRIPS',
            'GROSS_TO_ROYAL',
            'SIXTY_PERCENT',
            'TOTAL_CREDIT_CHARGES',
            'DRIVER_OWES',
            'TOTAL_TRIPS'
            
        );
    }
}
