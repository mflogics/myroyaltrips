<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estimate extends Model
{
    protected $fillable = [
        'customer_name',
        'customer_address',
        'customer_city',
        'customer_state',
        'customer_zip',
        'calculations_object',
        'created_by',
        'updated_by',
    ];

    public function creator()
    {
        return $this->belongsTo('App\User', 'created_by');
    }
    
    // public function getCalculationsAttribute(){
    //     return json_decode($this->calculations_object);
    // }

    public function updator()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
}
