<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TripSheet extends Model
{
    protected $fillable = [
        'mileage_start',
        'mileage_end',
        'note',
        'admin_note',
        'misc',
        'status',
        'car',
        'engine_oil',
        'transmission_fluid',
        'coolant',
        'user_id',
        'reservation_date',
        'payment_status',
    ];

    protected $dates = [
        'reservation_date',
    ];

    public function getCreatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(env('DATE_FORMAT'));
    }
    public function getReservationDateAttribute($date){
       if($date == '' || empty($date) || !isset($date)) {
           return $this->created_at;
       }
       
        return Carbon::createFromFormat('Y-m-d', $date)->format(env('DATE_FORMAT'));
    }

    public function setReservationDateAttribute($date){
        $this->attributes['reservation_date'] = Carbon::createFromFormat(env('DATE_FORMAT'), $date)->format('Y-m-d H:i:s');
    }

    public function getUpdatedAtAttribute($date){
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(env('DATE_FORMAT'));
    }
    // public function getCreatedAtAttribute($date){
    //     return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(env('DATE_FORMAT'));
    // }

    public function trips(){
        return $this->hasMany('App\Trip');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function getStats(){
        return Trip::getStats($this->trips);
    }

    public function getDriverOwesAttribute(){
        return $this->getStats()['DRIVER_OWES'];
    }
}
