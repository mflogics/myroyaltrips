<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estimate;
use Illuminate\Support\Facades\Auth;


class EstimateController extends Controller
{
    
    public function __construct()
    {
        $this->middleware(['auth', 'locale']);
    }
    
    public function welcome(){
        return view('welcome');
    }

    public function index()
    {
        if(Auth::user()->role!='admin'){
            $estimates = Estimate::where('created_by', Auth::id())->orderBy('created_at', 'desc')->paginate(50);
        }else{
            $estimates = Estimate::orderBy('created_at', 'desc')->paginate(50);
        }
        return view('estimate.index', compact('estimates'));
    }
    
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        // return $request->all();
        $estimate = new Estimate();
        $estimate->fill($request->all());
        $estimate->created_by = Auth::id();
        $estimate->save();
        $request->session()->flash('status', 'Estimate saved successfully!');
        return $this->index();
    }

    public function show($id)
    {
    }
    
    public function edit($id)
    {
        $estimate = Estimate::find($id);
        return view('estimate.edit', compact(['id']));
    }
    
    public function update(Request $request, $id)
    {
        $estimate =  Estimate::find($id);
        $estimate->fill($request->all());
        $estimate->updated_by = Auth::id();
        $estimate->save();
        $request->session()->flash('status', 'Updated successfully!');
        return redirect()->back();
    }

    public function destroy(Request $request, $id)
    {
        $estimate = Estimate::find($id);

        if (Auth::user()->role != 'admin' && Auth::id()!=$estimate->created_by  ) {
            return redirect()->back()->withErrors(['error' => 'You are not authorized to delete this estimate']);
        }

        Estimate::destroy($id);
        $request->session()->flash('status', 'Estimate deleted successfully!');
        return redirect()->back();

    }
}
