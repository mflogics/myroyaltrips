<?php

namespace App\Http\Controllers;

use App\Notifications\TripsheetApproved;
use App\Notifications\TripsheetRejected;
use App\Notifications\TripsheetSubmitted;
use App\Trip;
use App\TripSheet;
use App\User;
use DB;
use Excel;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Response;
use View;
use Carbon;

class TripController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

//    public function adjustDatabase (){
//        $data = DB::select("SELECT
//              t.id,
//              t.trip_sheet_id,
//              t.when,
//              ts.reservation_date
//            FROM
//              trips AS t
//              INNER JOIN trip_sheets AS ts
//                ON ts.id = t.trip_sheet_id
//            WHERE ts.reservation_date != DATE_FORMAT(t.`when`, '%Y-%m-%d')");
//        if(!empty($data)) {
//            foreach ($data as $datum) {
//                $trip_id = $datum->id;
//               $newWhenDateTime = '';
//               $whenDateTime = $datum->when;
//                if (!empty($whenDateTime)) {
//                    $whenDateArray = explode(' ', $whenDateTime);
//                     if (is_array($whenDateArray)) {
//                        $newWhenDateTime = date(env('DATE_FORMAT'), strtotime($datum->reservation_date . ' ' . $whenDateArray[1]));
//                     }
//                }
//                if(!empty($newWhenDateTime)) {
//                    $tripObj = Trip::find($trip_id);
//                    $tripObj->when = $newWhenDateTime;
//                    $tripObj->update();
//                }
//            }
//            die('records saved');
//        }
//    }

    public function setTripsheetStatus(Request $request, $id, $status)
    {
        $tripsheet = TripSheet::find($id);
        $tripsheet->status = $status;
        $tripsheet->save();

        if ($tripsheet->status == 'approved' && env('NOTIFICATIONS_ENABLED') == true) {
            // Auth::user()->notify(new TripsheetSubmitted($tripsheet));
            //$users = User::where('role', 'admin')->where('email','<>','admin@admin.com')->get();
            $tripsheet->user->notify(new TripsheetApproved($tripsheet));
        }

        if ($tripsheet->status == 'rejected' && env('NOTIFICATIONS_ENABLED') == true) {
            // Auth::user()->notify(new TripsheetSubmitted($tripsheet));
            //$users = User::where('role', 'admin')->where('email','<>','admin@admin.com')->get();
            $tripsheet->user->notify(new TripsheetRejected($tripsheet));
        }
        $request->session()->flash('status', 'Tripsheet Status updated successfully!');
        return redirect()->back();
    }

    public function updateTripsheetStatus(Request $request)
    {
        $id = $request->id;

        $tripsheet = TripSheet::find($id);
        $tripsheet->status = $request->status;
        $tripsheet->admin_note = $request->admin_note;
        $tripsheet->misc = $request->misc;
        $tripsheet->save();

        if ($tripsheet->status == 'approved' && env('NOTIFICATIONS_ENABLED') == true) {
            // Auth::user()->notify(new TripsheetSubmitted($tripsheet));
            //$users = User::where('role', 'admin')->where('email','<>','admin@admin.com')->get();
            $tripsheet->user->notify(new TripsheetApproved($tripsheet));
        }

        if ($tripsheet->status == 'rejected' && env('NOTIFICATIONS_ENABLED') == true) {
            // Auth::user()->notify(new TripsheetSubmitted($tripsheet));
            //$users = User::where('role', 'admin')->where('email','<>','admin@admin.com')->get();
            $tripsheet->user->notify(new TripsheetRejected($tripsheet));
        }
        $request->session()->flash('status', 'Tripsheet Status updated successfully!');
        return redirect()->back();
    }

    public function updateTripsheetBulkStatus(Request $request)
    {
//        $select_tripsheet_ids = explode(',' , rtrim((string) $request->select_tripsheet_id , ','));
//        echo "<pre>";
//        print_r($request->all());
//        die;
        foreach ($request->ids as $id) {
            $tripsheet = TripSheet::find($id);
            if ($request->status_value == "paid" || $request->status_value == "unpaid") {
                $tripsheet->payment_status = $request->status_value;
            } else {
                $tripsheet->status = $request->status_value;
            }
            $tripsheet->save();

            if ($tripsheet->status_value == 'approved' && env('NOTIFICATIONS_ENABLED') == true) {
                // Auth::user()->notify(new TripsheetSubmitted($tripsheet));
                //$users = User::where('role', 'admin')->where('email','<>','admin@admin.com')->get();
                $tripsheet->user->notify(new TripsheetApproved($tripsheet));
            }

            if ($tripsheet->status_value == 'rejected' && env('NOTIFICATIONS_ENABLED') == true) {
                // Auth::user()->notify(new TripsheetSubmitted($tripsheet));
                //$users = User::where('role', 'admin')->where('email','<>','admin@admin.com')->get();
                $tripsheet->user->notify(new TripsheetRejected($tripsheet));
            }
        }
        $request->session()->flash('status', 'Tripsheet Status updated successfully!');
        $return[] = [
            'status' => "success",
            'message' => 'Payment status updated.'
        ];
        return response()->json($return);
    }

    public function uploadCVSFile(Request $request)
    {
        DB::enableQueryLog();
        $request->validate([
            'driver_id' => 'required',
            'import_file' => 'required',
        ]);
        if ($request->hasFile('import_file')) {
            $path = $request->file('import_file')->getRealPath();
            $data = Excel::load($path, function ($reader) {
            })->get();
            $dataArray = [];
            if (!empty($data) && $data->count()) {
                foreach ($data->toArray() as $dataToEnter) {
                    if (!empty($dataToEnter['pickup_date'])) {
                        $pickupdateandtime = strtotime(trim($dataToEnter['pickup_date']));
                        $dateToEnter = date(env('DATE_FORMAT'), $pickupdateandtime);
                        $dateused = date('Y-m-d', $pickupdateandtime);
                        if (empty($dataArray[$dateused])) {
                            $dataArray[$dateused] = [
                                'user_id' => $request->driver_id,
                                'mileage_start' => 0,
                                'mileage_end' => 0,
                                'note' => $dataToEnter['party_notes'],
                                'car' => $dataToEnter['taxi'],
                                'status' => 'submitted',
                                'reservation_date' => $dateToEnter,
                            ];
                        }
                        $dataArray[$dateused]['trips'][] = [
                            'user_id' => $request->driver_id,
                            'when' => $dateToEnter,
                            'from' => $dataToEnter['pickup_location'],
                            'to' => $dataToEnter['dropoff_location'],
                            'voucher_number' => $dataToEnter['coupon_code'],
                            'customer' => $dataToEnter['party_name'],
                            'fees_cash' => $dataToEnter['fees_cash'],
                            'fees_credit' => $dataToEnter['fees_credit'],
                            'fare_cash' => $dataToEnter['fare_cash'],
                            'voucher_amount' => $dataToEnter['fare_ccvoucher_v'],
                            'tip' => $dataToEnter['ccv_tips'],
                        ];
                        $trip_sheet_id = 0;

//
                    }
                }
                if (!empty($dataArray)) {
                    foreach ($dataArray as $key => $datum) {
                        $queryTo = "SELECT * FROM `trip_sheets` WHERE `reservation_date` LIKE '%" . $key . "%' AND `user_id` = " . $request->driver_id . " LIMIT 1";
                        $dataExit = DB::select($queryTo);
//                    dd(DB::getQueryLog());
                        if (!empty($dataExit)) {
                            $trip_sheet_id = $dataExit[0]->id;
                        } else {
                            $tripSheetObj = new TripSheet();
                            $tripSheetObj->user_id = $request->driver_id;
                            $tripSheetObj->mileage_start = 0;
                            $tripSheetObj->mileage_end = 0;
                            $tripSheetObj->note = $datum['note'];
                            $tripSheetObj->car = $datum['car'];
                            $tripSheetObj->status = 'submitted';
                            $tripSheetObj->reservation_date = $datum['reservation_date'];
                            $tripSheetObj->save();
                            $trip_sheet_id = $tripSheetObj->id;
                        }
                        foreach ($datum['trips'] as $trip) {
                            $tripObj = new Trip();
                            $tripObj->user_id = $request->driver_id;
                            if (!empty($trip['when'])) {
                                $tripObj->when = $trip['when'];
                            } else {
                                $tripObj->when = 'NULL';
                            }
                            if (!empty($trip['from'])) {
                                $tripObj->from = $trip['from'];
                            } else {
                                $tripObj->from = 'NULL';
                            }
                            if (!empty($trip['to'])) {
                                $tripObj->to = $trip['to'];
                            } else {
                                $tripObj->to = 'NULL';
                            }
                            $tripObj->trip_sheet_id = $trip_sheet_id;
                            if (!empty($trip['voucher_number'])) {
                                $tripObj->voucher_number = $trip['voucher_number'];
                            } else {
                                $tripObj->voucher_number = "NULL";
                            }
                            if (!empty($trip['fees_cash'])) {
                                $tripObj->fees_cash = $trip['fees_cash'];
                            } else {
                                $tripObj->fees_cash = 0.00;
                            }
                            if (!empty($trip['voucher_amount'])) {
                                $tripObj->voucher_amount = $trip['voucher_amount'];
                            } else {
                                $tripObj->voucher_amount = 0.00;
                            }
                            if (!empty($trip['fees_credit'])) {
                                $tripObj->fees_credit = $trip['fees_credit'];
                            } else {
                                $tripObj->fees_credit = 0.00;
                            }
                            if (!empty($trip['fare_cash'])) {
                                $tripObj->fare_cash = $trip['fare_cash'];
                            } else {
                                $tripObj->fare_cash = 0.00;
                            }
                            if (!empty($trip['tip'])) {
                                $tripObj->tip = $trip['tip'];
                            } else {
                                $tripObj->tip = 0.00;
                            }
                            if ($trip['customer']) {
                                $tripObj->customer = $trip['customer'];
                            } else {
                                $tripObj->customer = "NULL";
                            }
                            $tripObj->save();
                        }
                    }

                }
                return back()->with('status', 'Records Inserted successfully.');
            }
        }
        return back()->with('error', 'Please Check your file, Something is wrong there.');
    }

    public function CompareCVSFile(Request $request)
    {
        DB::enableQueryLog();
        $request->validate([
//            'driver_id' => 'required',
            'status' => 'required',
            'import_file' => 'required',
        ]);
        $driver_id = 0;
        if ($request->hasFile('import_file')) {
            $path = $request->file('import_file')->getRealPath();
            $data = Excel::load($path, function ($reader) {
            })->get();
            $dataArray = [];
            if (!empty($data) && $data->count()) {
                foreach ($data->toArray() as $dataToEnter) {
                    if (empty($request->driver_id)) {
                        $selected_driver_id = 0;
                    } else {
                        $selected_driver_id = $request->driver_id;
                    }
                    $user = DB::table('users')->where('name', '=', trim($dataToEnter['driver']))->get()->toArray();
                    if (!empty($user)) {
                        $driver_id = $user[0]->id;
                    } else {
                        $driver_id = 0;
                    }

                    if (!empty($dataToEnter['pickup_date']) && !empty($driver_id)) {
                        if((empty($selected_driver_id)) || (!empty($selected_driver_id) && $driver_id == $selected_driver_id)) {
                            $pickupdateandtime = strtotime(trim($dataToEnter['pickup_date']));
                            $dateToEnter = date(env('DATE_FORMAT'), $pickupdateandtime);
                            $dateused = date('Y-m-d', $pickupdateandtime);
                            if (empty($dataArray[$driver_id][$dateused])) {
                                $dataArray[$driver_id][$dateused] = [
                                    'user_id' => $driver_id,
                                    'mileage_start' => 0,
                                    'mileage_end' => 0,
                                    'note' => $dataToEnter['party_notes'],
                                    'car' => $dataToEnter['taxi'],
                                    'status' => 'submitted',
                                    'reservation_date' => $dateToEnter,
                                ];
                            }
                            $dataArray[$driver_id][$dateused]['trips'][] = [
                                'user_id' => $driver_id,
                                'when' => $dateToEnter,
                                'from' => $dataToEnter['pickup_location'],
                                'to' => $dataToEnter['dropoff_location'],
                                'voucher_number' => $dataToEnter['coupon_code'],
                                'customer' => $dataToEnter['party_name'],
                            ];
                        }
                    }
                }

                if (!empty($dataArray)) {
                    foreach ($dataArray as $key => $datum) {
                        foreach ($datum as $date => $datum2) {
                            $trip_sheet_id = 0;
                            $queryTo = "SELECT * FROM `trip_sheets` WHERE `reservation_date` LIKE '%" . $date . "%'";
                            if ($request->status == 'all') {
//                                $queryTo .= " AND `status` IN()= 'missing'";
                            } else {
                                $queryTo .= " AND (`status` = '" . $request->status . "' OR `status` = 'missing')";
                            }
                            $queryTo .= " AND `user_id` = " . $key . " LIMIT 1";
                            $dataExit = DB::select($queryTo);
//                    dd(DB::getQueryLog());
                            if (empty($dataExit)) {
                                $tripSheetObj = new TripSheet();
                                $tripSheetObj->user_id = $key;
                                $tripSheetObj->mileage_start = 0;
                                $tripSheetObj->mileage_end = 0;
                                $tripSheetObj->note = $datum2['note'];
                                $tripSheetObj->car = $datum2['car'];
                                $tripSheetObj->status = 'missing';
                                $tripSheetObj->reservation_date = $datum2['reservation_date'];
                                $tripSheetObj->save();
                                $trip_sheet_id = $tripSheetObj->id;
                            }
                            if (!empty($trip_sheet_id)) {
                                foreach ($datum2['trips'] as $trip) {
                                    $tripObj = new Trip();
                                    $tripObj->user_id = $key;
                                    if (!empty($trip['when'])) {
                                        $tripObj->when = $trip['when'];
                                    } else {
                                        $tripObj->when = 'NULL';
                                    }
                                    if (!empty($trip['from'])) {
                                        $tripObj->from = $trip['from'];
                                    } else {
                                        $tripObj->from = 'NULL';
                                    }
                                    if (!empty($trip['to'])) {
                                        $tripObj->to = $trip['to'];
                                    } else {
                                        $tripObj->to = 'NULL';
                                    }
                                    $tripObj->trip_sheet_id = $trip_sheet_id;
                                    if (!empty($trip['voucher_number'])) {
                                        $tripObj->voucher_number = $trip['voucher_number'];
                                    } else {
                                        $tripObj->voucher_number = "NULL";
                                    }
                                    if (!empty($trip['fees_cash'])) {
                                        $tripObj->fees_cash = $trip['fees_cash'];
                                    } else {
                                        $tripObj->fees_cash = 0.00;
                                    }
                                    if (!empty($trip['voucher_amount'])) {
                                        $tripObj->voucher_amount = $trip['voucher_amount'];
                                    } else {
                                        $tripObj->voucher_amount = 0.00;
                                    }
                                    if (!empty($trip['fees_credit'])) {
                                        $tripObj->fees_credit = $trip['fees_credit'];
                                    } else {
                                        $tripObj->fees_credit = 0.00;
                                    }
                                    if (!empty($trip['fare_cash'])) {
                                        $tripObj->fare_cash = $trip['fare_cash'];
                                    } else {
                                        $tripObj->fare_cash = 0.00;
                                    }
                                    if (!empty($trip['tip'])) {
                                        $tripObj->tip = $trip['tip'];
                                    } else {
                                        $tripObj->tip = 0.00;
                                    }
                                    if ($trip['customer']) {
                                        $tripObj->customer = $trip['customer'];
                                    } else {
                                        $tripObj->customer = "NULL";
                                    }
                                    $tripObj->save();
                                }
                            }
                        }
                    }

                }
                return back()->with('status', 'Records Inserted successfully.');
            }
            if (!empty($data) && $data->count()) {
                foreach ($data->toArray() as $dataToEnter) {
                    if (empty($request->driver_id)) {
                        $selected_driver_id = 0;
                    } else {
                        $selected_driver_id = $request->driver_id;
                    }
                    $user = DB::table('users')->where('name', '=', trim($dataToEnter['driver']))->get()->toArray();
                    if (!empty($user)) {
                        $driver_id = $user[0]->id;
                    } else {
                        $driver_id = 0;
                    }

                    if (!empty($dataToEnter['pickup_date']) && !empty($driver_id)) {
                        if((empty($selected_driver_id)) || (!empty($selected_driver_id) && $driver_id == $selected_driver_id)) {
                            $pickupdateandtime = strtotime(trim($dataToEnter['pickup_date']));
                            $dateToEnter = date(env('DATE_FORMAT'), $pickupdateandtime);
                            $dateused = date('Y-m-d', $pickupdateandtime);
                            if (empty($dataArray[$driver_id][$dateused])) {
                                $dataArray[$driver_id][$dateused] = [
                                    'user_id' => $driver_id,
                                    'mileage_start' => 0,
                                    'mileage_end' => 0,
                                    'note' => $dataToEnter['party_notes'],
                                    'car' => $dataToEnter['taxi'],
                                    'status' => 'submitted',
                                    'reservation_date' => $dateToEnter,
                                ];
                            }
                            $dataArray[$driver_id][$dateused]['trips'][] = [
                                'user_id' => $driver_id,
                                'when' => $dateToEnter,
                                'from' => $dataToEnter['pickup_location'],
                                'to' => $dataToEnter['dropoff_location'],
                                'voucher_number' => $dataToEnter['coupon_code'],
                                'customer' => $dataToEnter['party_name'],
                            ];
                        }
                    }
                }

                if (!empty($dataArray)) {
                    foreach ($dataArray as $key => $datum) {
                        foreach ($datum as $date => $datum2) {
                            $trip_sheet_id = 0;
                            $queryTo = "SELECT * FROM `trip_sheets` WHERE `reservation_date` LIKE '%" . $date . "%'";
                            if ($request->status == 'all') {
                                $queryTo .= " AND `status` != 'missing'";
                            } else {
                                $queryTo .= " AND (`status` = '" . $request->status . "' AND `status` != 'missing')";
                            }
                            $queryTo .= " AND `user_id` = " . $key . " LIMIT 1";
                            $dataExit = DB::select($queryTo);
//                    dd(DB::getQueryLog());
                            if (empty($dataExit)) {
                                $tripSheetObj = new TripSheet();
                                $tripSheetObj->user_id = $key;
                                $tripSheetObj->mileage_start = 0;
                                $tripSheetObj->mileage_end = 0;
                                $tripSheetObj->note = $datum2['note'];
                                $tripSheetObj->car = $datum2['car'];
                                $tripSheetObj->status = 'missing';
                                $tripSheetObj->reservation_date = $datum2['reservation_date'];
                                $tripSheetObj->save();
                                $trip_sheet_id = $tripSheetObj->id;
                            }
                            if (!empty($trip_sheet_id)) {
                                foreach ($datum2['trips'] as $trip) {
                                    $tripObj = new Trip();
                                    $tripObj->user_id = $key;
                                    if (!empty($trip['when'])) {
                                        $tripObj->when = $trip['when'];
                                    } else {
                                        $tripObj->when = 'NULL';
                                    }
                                    if (!empty($trip['from'])) {
                                        $tripObj->from = $trip['from'];
                                    } else {
                                        $tripObj->from = 'NULL';
                                    }
                                    if (!empty($trip['to'])) {
                                        $tripObj->to = $trip['to'];
                                    } else {
                                        $tripObj->to = 'NULL';
                                    }
                                    $tripObj->trip_sheet_id = $trip_sheet_id;
                                    if (!empty($trip['voucher_number'])) {
                                        $tripObj->voucher_number = $trip['voucher_number'];
                                    } else {
                                        $tripObj->voucher_number = "NULL";
                                    }
                                    if (!empty($trip['fees_cash'])) {
                                        $tripObj->fees_cash = $trip['fees_cash'];
                                    } else {
                                        $tripObj->fees_cash = 0.00;
                                    }
                                    if (!empty($trip['voucher_amount'])) {
                                        $tripObj->voucher_amount = $trip['voucher_amount'];
                                    } else {
                                        $tripObj->voucher_amount = 0.00;
                                    }
                                    if (!empty($trip['fees_credit'])) {
                                        $tripObj->fees_credit = $trip['fees_credit'];
                                    } else {
                                        $tripObj->fees_credit = 0.00;
                                    }
                                    if (!empty($trip['fare_cash'])) {
                                        $tripObj->fare_cash = $trip['fare_cash'];
                                    } else {
                                        $tripObj->fare_cash = 0.00;
                                    }
                                    if (!empty($trip['tip'])) {
                                        $tripObj->tip = $trip['tip'];
                                    } else {
                                        $tripObj->tip = 0.00;
                                    }
                                    if ($trip['customer']) {
                                        $tripObj->customer = $trip['customer'];
                                    } else {
                                        $tripObj->customer = "NULL";
                                    }
                                    $tripObj->save();
                                }
                            }
                        }
                    }

                }
                return back()->with('status', 'Records Inserted successfully.');
            }
        }
        return back()->with('error', 'Please Check your file, Something is wrong there.');
    }

    public function updateTripsheetPaymentStatus(Request $request)
    {
        $id = $request->trip_sheet_id;

        $tripsheet = TripSheet::find($id);
        $tripsheet->payment_status = $request->payment_status;
        $tripsheet->save();
        $return[] = [
            'status' => "success",
            'message' => 'Payment status updated.'
        ];
        return response()->json($return);
    }



    // public function getTripsheet($filter){
    //     $is_admin = Auth::user()->role == 'admin';
    //     $status = [ 'new' => 'submitted', 'approved' => 'approved', 'rejected' =>'rejected' ];
    //     $tripsheets =  $tripsheets = TripSheet::where('status', $status[$filter])->get();
    //     dd($tripsheets);
    //     return view('trip.history', compact('tripsheets', 'is_admin', 'filter'));

    // }

    private function _getTrips()
    {
        return Trip::where('user_id', Auth::id())->whereNull('trip_sheet_id')->get();
    }

    public function home()
    {

        $recent_tripsheets = [];
        $submitted_tripsheets = [];

        if (Auth::user()->role == 'user') {

            $trips = $this->_getTrips();
            $stats = Trip::getStats($trips);
            $recent_tripsheets = TripSheet::where('user_id', Auth::id())->orderBy('updated_at', 'desc')->take(10)->get();

        }

        if (Auth::user()->role == 'admin') {

            $new_tripsheet_count = TripSheet::where('status', 'submitted')->count();
            $approved_tripsheet_count = TripSheet::where('status', 'approved')->count();
            $rejected_tripsheet_count = TripSheet::where('status', 'rejected')->count();
            $total_tripsheet_count = $new_tripsheet_count + $approved_tripsheet_count + $rejected_tripsheet_count;


            $stats = [
                'TOTAL_TRIPSHEETS' => $total_tripsheet_count,
                'NEW_TRIPSHEETS' => $new_tripsheet_count,
                'APPROVED' => $approved_tripsheet_count,
                'REJECTED' => $rejected_tripsheet_count,
            ];

            $recent_tripsheets = TripSheet::orderBy('updated_at', 'desc')->take(10)->get();
            $submitted_tripsheets = TripSheet::where('status', 'submitted')->get()->sortByDesc('driver_owes');
            // $submitted_tripsheets->sortBy('driver_owes');
        }

        if (Auth::user()->role == 'super_admin') {
            $stats = [
                'TOTAL_TRIPSHEETS' => TripSheet::count(),
                'NEW_TRIPSHEETS' => TripSheet::where('status', 'submitted')->count(),
                'APPROVED' => TripSheet::where('status', 'approved')->count(),
                'REJECTED' => TripSheet::where('status', 'rejected')->count(),
                'DELETED' => Tripsheet::where('status', 'deleted')->count(),
            ];

            $recent_tripsheets = TripSheet::orderBy('updated_at', 'desc')->take(10)->get();
            $submitted_tripsheets = TripSheet::where('status', 'submitted')->get()->sortByDesc('driver_owes');
            // $submitted_tripsheets->sortBy('driver_owes');
        }

        return view('home', compact('stats', 'recent_tripsheets', 'submitted_tripsheets'));
    }

    public function submitTripsheet(Request $request)
    {
        $request->validate([
            'mileage_start' => 'required',
            'mileage_end' => 'required',
            'car' => 'required',
            'reservation_date' => 'required',
        ]);

        $date = (\DateTime::createFromFormat('m-d-Y', $request->reservation_date))->format("Y-m-d");

        $exist = TripSheet::where('user_id', Auth::id())->where('reservation_date', $date)->first();
        if ($exist) {
            $request->session()->flash('status', 'Duplicate Tripsheet!');
            return redirect('/');
        } else {
            $tripsheet = new TripSheet;
            $tripsheet->fill($request->all());
            $trips = $this->_getTrips();
            // $tripsheet->trips->add($trips);
            $tripsheet->user_id = Auth::id();
            $tripsheet->status = "submitted";


            $tripsheet->save();

            foreach ($trips as $trip) {
                $trip->trip_sheet_id = $tripsheet->id;
                $trip->save();
            }

            // Auth::user()->notify(new TripsheetSubmitted($tripsheet));
            if (env('NOTIFICATIONS_ENABLED') == true) {
                $users = User::where('role', 'admin')->where('email', '<>', 'admin@admin.com')->get();
                Notification::send($users, new TripsheetSubmitted($tripsheet));
            }

            $request->session()->flash('status', 'Tripsheet submitted successfully!');
            return redirect('/');
        }
    }

    public function updateTripsheet(Request $request, $id)
    {
        $request->validate([
            'mileage_start' => 'required',
            'mileage_end' => 'required',
            'car' => 'required',
        ]);
        $tripsheet = TripSheet::find($id);
        $tripsheet->fill($request->all());
        $tripsheet->save();

        $request->session()->flash('status', 'Tripsheet updated successfully!');
        return redirect('/');

    }

    public function index(Request $request)
    {
        $sheet = $request->get('sheet');
        $sheetdate = '';
        $trips = $this->_getTrips();
        if (count($trips) > 0) {
            $sheetdate = $trips->first()->when;
        }

        // $trips = Trip::where('user_id', Auth::id())->orderBy('created_at', 'desc')->paginate(100);
        return view('trip.index', compact('trips', 'sheet', 'sheetdate'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {

        if ($request->hasFile('imm') && $request->file('imm')->isValid()) {
            $path = $request->imm->store('images');
            rename("/home3/myroyal8/myroyaltrips/storage/app/" . $path, "/home3/myroyal8/public_html/" . $path);
        } else {
            $path = "";
        }
        $edit = false;

        $sheet = $request->trip_sheet_id;

        $appendRule = '';
        $messages = [];
        if (empty($sheet)) {
            //fetch any trip in currrent trip sheet and compare the date
            $anyTrip = $trip = \App\Trip::where('user_id', Auth::id())->whereNull('trip_sheet_id')->first();
            if ($anyTrip != null) {
                $appendRule = '|date_equals:' . $anyTrip->when;//getAttributes()['when'];
            }

            //return $appendRule;

            $messages = [
                'when.date_equals' => 'Date should be same for all the trips in current tripsheet'
            ];
        }

        $request->validate([
            'customer' => 'required',
            'from' => 'required',
            'to' => 'required',
            'when' => 'required' . $appendRule,
            'fees_cash' => 'required',
            'fees_credit' => 'required',
            'fare_cash' => 'required',
            'voucher_amount' => 'required',
            // 'voucher_number' => 'required',
            'tip' => 'required',
        ], $messages);

        $trip = new Trip;
        $trip->fill($request->all());

        if (!empty($path)) {
            $trip->image = $path;
        }

        $trip->user_id = Auth::id();
        $trip->save();
        // Trip::create($request->all());
        $request->session()->flash('status', 'Trip added successfully!');

        if (!empty($sheet)) {
            return redirect()->route('tripsheet.history', ['filter' => 'rejected']);
        }

        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $trip = Trip::find($id);
        $sheetdate = $trip->when;
        $trips = $this->_getTrips();
        return view('trip.index', compact('trips', 'trip', 'sheetdate'));
    }

    public function update(Request $request, $id)
    {
        if ($request->hasFile('imm') && $request->file('imm')->isValid()) {
            $path = $request->imm->store('images');
            rename("/home3/myroyal8/myroyaltrips/storage/app/" . $path, "/home3/myroyal8/public_html/" . $path);
        } else {
            $path = "";
        }
        $appendRule = '';
        //fetch any trip in currrent trip sheet and compare the date
        $anyTrip = $trip = \App\Trip::where('user_id', Auth::id())->whereNull('trip_sheet_id')->first();
        // if($anyTrip != null){
        //     $appendRule = '|date_equals:'.$anyTrip->when;//getAttributes()['when'];
        // }

        //return $appendRule;

        $messages = [
            'when.date_equals' => 'Date should be same for all the trips in current tripsheet'
        ];
        $request->validate([
            'customer' => 'required',
            'from' => 'required',
            'to' => 'required',
            'fees_cash' => 'required',
            'fees_credit' => 'required',
            'fare_cash' => 'required',
            'voucher_amount' => 'required',
            // 'voucher_number' => 'required',
            'tip' => 'required',
        ], $messages);

        $trip = Trip::find($id);
        $trip->fill($request->all());
        if (!empty($path)) {
            if (!empty($trip->image)) {
                unlink(" / home3 / myroyal8 / public_html / " . $trip->image);
            }
            $trip->image = $path;
        }
        if (Auth::user()->role != 'admin') {
            $trip->user_id = Auth::id();
        }
        $trip->save();
        $request->session()->flash('status', 'Trip updated successfully!');
        return redirect()->back();

    }

    private function _getTripsheets($fromDate = null, $toDate = null, $status = null, $driver_id = null, $payment_status = null, $order = null, $order_by_user = null)
    {
        DB::enableQueryLog();
        $query = TripSheet::select('*');
        if (!(empty($fromDate) && empty($toDate))) {
            $query = TripSheet::whereBetween('reservation_date', [$fromDate, $toDate]);
        }

        if (!empty($payment_status) && $payment_status != 'all') {
            if ($payment_status == 'paid' || $payment_status == 'unpaid') {
                $query->where('payment_status', $payment_status);
            } else {
                $query->where('status', $payment_status);
            }
        }
        if(!empty($order_by_user)){
            $query->join('users', 'trip_sheets.user_id', '=', 'users.id')->orderBy('users.name', $order_by_user);

        }
        $query->orderBy('reservation_date', $order);
        if (empty($payment_status)) {
            if ($status && $status !== 'all') {
                $query->where('status', $status);
            }
        }

        if (Auth::user()->role == 'user') {
            $query->where('user_id', Auth::id());
        }


        if ($driver_id) {
            $query->where('user_id', $driver_id);
        }


        return $query;//test


        if (empty($fromDate) || empty($toDate)) {
            if (Auth::user()->role == 'user') {
                return TripSheet::where('user_id', Auth::id())->get();
            } else {
                if ($status) {
                    return TripSheet::where('status', $status)->get();
                }
                return TripSheet::paginate(2);

            }
        }

        if (Auth::user()->role == 'user') {
            return TripSheet::where('user_id', Auth::id())->whereBetween('created_at', [$fromDate, $toDate])->get();
        } else {
            if ($status) {
                return TripSheet::where('status', $status)->whereBetween('created_at', [$fromDate, $toDate])->get();
            }
            return TripSheet::whereBetween('created_at', [$fromDate, $toDate])->get();
        }


    }

    public function history(Request $request, $filter = null)
    {
        DB::enableQueryLog();
        if (!empty($request->from) && !empty($request->to)) {
        $request->validate([
            'from' => 'date_format:' . env('DATE_FORMAT'),
            'to' => 'date_format:' . env('DATE_FORMAT'),
        ]);
        }
//        echo "<pre>";
//        print_r($request->order_by_user);
//        die;
        $is_admin = Auth::user()->role == 'admin';
        $is_super_admin = Auth::user()->role == 'super_admin';
        $pagi = [];

        if ($request->from) {
            $pagi['from'] = $request->from;
        }
        if ($request->to) {
            $pagi['to'] = $request->to;
        }

        if ($request->driver_id) {
            $pagi['driver_id'] = $request->driver_id;
        }

        if (!empty($request->payment_status) && ($request->payment_status == 'paid' || $request->payment_status == 'unpaid')) {
            $pagi['payment_status'] = $request->payment_status;
        }


        $from = empty($request->from) ? null : Carbon::createFromFormat(env('DATE_FORMAT'), $request->from)->subDay()->format('Y-m-d H:i:s');
        $to = empty($request->to) ? null : Carbon::createFromFormat(env('DATE_FORMAT'), $request->to)->addDay()->format('Y-m-d H:i:s');
        $driver_id = empty($request->driver_id) ? null : $request->driver_id;
        $payment_status = empty($request->payment_status) ? null : $request->payment_status;

        //just for summary calculations
        $all = $this->_getTripsheets($from, $to, $filter, $driver_id, $payment_status)->get();
        $total_owes = 0;
        $submitted = 0;
        $rejected = 0;
        $missing = 0;
        $approved = 0;
        $deleted = 0;
        foreach ($all as $one) {
            $total_owes += ($one->getStats()['DRIVER_OWES'] - $one->misc);
            if($one->status == 'submitted'){
                $submitted++;
        }
            if($one->status == 'rejected'){
                $rejected++;
            }
            if($one->status == 'missing'){
                $missing++;
            }
            if($one->status == 'approved'){
                $approved++;
            }
            if($one->status == 'deleted'){
                $deleted++;
            }

        }
        $tripsheets = $this->_getTripsheets($from, $to, $filter, $driver_id, $payment_status, $request->order_by, $request->order_by_user)->paginate(100);

        $drivers = User::where('role', 'user')->get();
        return view('trip.history', compact('tripsheets', 'is_admin', 'filter', 'drivers', 'total_owes', 'pagi', 'is_super_admin', 'submitted', 'rejected', 'missing', 'approved', 'deleted'));
        return 'previously submitted trip sheets will be visible here';
    }

    public function adminNoteUpdate (Request $request){
        $tripsheet = TripSheet::find($request->trip_sheet_id);
        $tripsheet->admin_note = $request->admin_note;
        $tripsheet->save();
        return back()->with('status', 'Records Updated successfully.');
    }

    public function download_csv_file(Request $request, $filter = null)
    {

        $from = empty($request->from) ? null : Carbon::createFromFormat(env('DATE_FORMAT'), $request->from)->subDay()->format('Y-m-d H:i:s');
        $to = empty($request->to) ? null : Carbon::createFromFormat(env('DATE_FORMAT'), $request->to)->addDay()->format('Y-m-d H:i:s');
        $driver_id = empty($request->driver_id) ? null : $request->driver_id;

        $tripsheets = $this->_getTripsheets($from, $to, $filter, $driver_id)->get();

        $csv_data = ['Driver', 'TripsheetDate', 'Status', 'Date', 'MileageFrom', 'MileageTo', 'Car', 'EngineOil', 'TransmissionFluid', 'Coolant', 'Note', 'AdminNote', 'Trips', 'DriverOwes', 'GrossSale', 'TotalFees', 'TotalCashCreditFare', 'DriverSubTotal', 'TotalCreditFaresTips', 'GrossToRoyal', 'SixtyPercent', 'MisecellneousChargeCredit'];
        $data = $this->arrayToCsv($csv_data);

        foreach ($tripsheets as $one) {
            $user_name = ($one->user->name);
            $csv_data[0] = $user_name;
            $reservation_date = ($one->reservation_date);
            $csv_data[1] = $reservation_date;
            $status = ($one->status);
            $csv_data[2] = $status;
            $updated_at = ($one->updated_at);
            $csv_data[3] = $updated_at;
            $mileage_start = ($one->mileage_start);
            $csv_data[4] = $mileage_start;
            $mileage_end = ($one->mileage_end);
            $csv_data[5] = $mileage_end;
            $car = ($one->car);
            $csv_data[6] = $car;
            $engine_oil = ($one->engine_oil);
            $csv_data[7] = $engine_oil;
            $transmission_oil = ($one->transmission_fluid);
            $csv_data[8] = $transmission_oil;
            $coolant = ($one->coolant);
            $csv_data[9] = $coolant;
            $note = ($one->note);
            $csv_data[10] = $note;
            $admin_note = ($one->admin_note);
            $csv_data[11] = $admin_note;

            $stats = \App\Trip::getStats($one->trips);

            $trips = $one->trips->count();
            $csv_data[12] = $trips;
            $driver_owes = ($stats['DRIVER_OWES'] - $one->misc);
            $csv_data[13] = $driver_owes;
            $gross_sale = $stats['GROSS_SALE'];
            $csv_data[14] = $gross_sale;
            $total_fees = $stats['TOTAL_FEES'];
            $csv_data[15] = $total_fees;
            $total_cash_credit = $stats['TOTAL_CASH_CREDIT_FARE'];
            $csv_data[16] = $total_cash_credit;
            $driver_subtotal = $stats['DRIVER_OWES'];
            $csv_data[17] = $driver_subtotal;
            $total_credit_tips = $stats['TOTAL_CREDIT_FARE_TRIPS'];
            $csv_data[18] = $total_credit_tips;
            $gross_to_royal = $stats['GROSS_TO_ROYAL'];
            $csv_data[19] = $gross_to_royal;
            $sixty_percent = $stats['SIXTY_PERCENT'];
            $csv_data[20] = $sixty_percent;
            $misc = $one->misc;
            $csv_data[21] = $misc;

            $data .= $this->arrayToCsv($csv_data);
        }


        $fileName = date('r') . '_' . $filter . '_datafile.csv';
        File::put(public_path('/download/' . $fileName), $data);
        return Response::download(public_path('/download/' . $fileName));
    }

    /**
     * Formats a line (passed as a fields  array) as CSV and returns the CSV as a string.
     * Adapted from http://us3.php.net/manual/en/function.fputcsv.php#87120
     */
    private function arrayToCsv(array $fields, $delimiter = ',', $enclosure = '"', $encloseAll = false, $nullToMysqlNull = false)
    {
        $delimiter_esc = preg_quote($delimiter, ' / ');
        $enclosure_esc = preg_quote($enclosure, ' / ');

        $outputString = "";
        $output = array();
        foreach ($fields as $field) {


            // ADDITIONS BEGIN HERE
            if (gettype($field) == 'integer' || gettype($field) == 'double') {
                $field = strval($field); // Change $field to string if it's a numeric type
            }

            // ADDITIONS END HERE
            if ($field === null && $nullToMysqlNull) {
                $output[] = 'NULL';
                continue;
            }

            // Enclose fields containing $delimiter, $enclosure or whitespace
            if ($encloseAll || preg_match("/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field)) {
                $field = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure;
            }

            $output[] = $field;
        }

        $outputString .= implode($delimiter, $output);
        return $outputString . "\r\n";
    }

    public
    function destroy($id, Request $request)
    {
        $trip = Trip::find($id);

        if ($trip->user_id != Auth::id() && Auth::user()->role != 'admin') {
            return redirect()->back()->withErrors(['error' => 'You are not authorized to delete this trip']);
        }

        Trip::destroy($id);
        $request->session()->flash('status', 'Trip Entry deleted successfully!');
        return redirect()->back();
    }

    public
    function destroyTripsheet($id, Request $request)
    {
        if (Auth::user()->role != 'admin') {
            return redirect()->back()->withErrors(['error' => 'You are not authorized to delete this tripsheet']);
        }
        TripSheet::destroy($id);
        $request->session()->flash('status', 'Trip Sheet Entry deleted successfully!');
        return redirect()->back();
    }
}
