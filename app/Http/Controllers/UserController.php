<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Akaunting\Setting;
class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function setLocale( $locale){
        $user = Auth::user();
        $user->locale = $locale;
        $user->save();
        return redirect()->back();
    }


    public function getSettings(Request $request){
        if( in_array($request->currency, ['EUR', 'USD', 'GBP'] ) ){
           $user = User::find(Auth::id());
           $user->currency = $request->currency;
           $user->save();
           return redirect()->back();

        }
        return view('user.settings');
    }

    public function postSettings(Request $request){
        $request->validate([
            'USD' => 'required|numeric',
            'GBP' => 'required|numeric',
        ]);

        \Setting::set('USD', $request->USD);
        \Setting::set('GBP', $request->GBP);
        \Setting::save();
    
        $request->session()->flash('status', 'Settings saved successfully!');
        return redirect()->back();
    }

    private function _getUsers(){
        if(Auth::user()->role == 'user'){
            return  [User::find(Auth::id())];
        }
        
        
        return User::all();
       
        
    }
   
    public function index()
    {
        if(Auth::user()->role == 'user'){
           return redirect(action('UserController@edit', Auth::id()));
        }

        $users = $this->_getUsers();
        return view('user.index', compact('users'));
    }

    public function create()
    {
        //
    }

   
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'role' => 'required|in:admin,user,super_admin',
            // 'hourly_rate' => 'required|numeric',
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'role' => $request->role,
            'hourly_rate' => 0,
            'password' => Hash::make($request->password),
        ]);

        $request->session()->flash('status', 'User created successfully!');
        return redirect()->back();

    }

   
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        $user = User::find($id);
        $users = $this->_getUsers();
        return view('user.index', compact('users', 'user'));
    }

    public function update(Request $request, $id)
    {
       $request->validate([
           'name' => 'required|string|max:255',
           'email' => 'required|string|email|max:255|unique:users,email,'.$id,
           'role' => 'required|in:admin,user,super_admin',
           'password' => 'nullable|string|min:6|confirmed',
        //    'hourly_rate' => 'required|numeric',
       ]);

       $user = User::find($id);
       $user->name = $request->name;

       $user->email = $request->email;

       $user->role = $request->role;
       $user->hourly_rate = 0;

       if(!empty($request->password)){
            $user->password = Hash::make($request->password);
        }

        $user->save();
        $request->session()->flash('status', 'User updated successfully!');
        return redirect()->back();
    }

    
    public function destroy($id, Request $request)
    {
        if(Auth::user()->role != 'admin' && Auth::user()->role != 'super_admin'){
            return redirect()->back()->withErrors(['error'=> 'Only admin can delete a user']);
        }

        if(Auth::id() == $id){
            return redirect()->back()->withErrors(['error'=> 'You can not delete your own account']);
        }
        
        if(User::count() == 1){
            return redirect()->back()->withErrors(['error'=> 'You are the only user left, so cannot be deleted']);
        }

        User::destroy($id);
        $request->session()->flash('status', 'User deleted successfully!');
        return redirect()->back();
    }
}
