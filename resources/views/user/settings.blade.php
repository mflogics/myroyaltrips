@extends('layouts.app')

@section('content')

            <div class="card border-primary">
                
                <div class="card-body">
                    <legend class="text-primary">Currency Settings</legend>
                    <div class="alert alert-default bg-light" role="alert">
                        <strong>  Euro is the base currency</strong>
                    </div>
                    <form method="post" action="{{url('settings')}}">
                    {{csrf_field()}}
                    <div class="form-group">
                      <label for="">Dollar (USD)</label>
                      <input name="USD" value="{{\Setting::get('USD', '')}}" type="number" step="0.001" class="form-control"  placeholder="how many USD in 1 EUR">
                    </div>
               
                    <div class="form-group">
                      <label for="">Sterling (GBP)</label>
                      <input name="GBP" value="{{\Setting::get('GBP', '')}}" type="number" step="0.001" class="form-control"  placeholder="how many GBP in 1 EUR">
                    </div>

                    <button type="submit" class="btn btn-primary">
                        Save
                    </button>

                    </form>
                  
               
                </div>
            </div>
   
@endsection
