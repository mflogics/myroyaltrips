@extends('layouts.app')

@section('content')

            <div class="card @if(isset($user)) border-warning @else border-primary @endif">
                <!-- <div class="card-header">users</div> -->

                <div class="card-body">
                 
                    @if(isset($user))

                <form action="{{route('user.update', $user->id)}}" method="POST" role="form">
                    @if($user->role =='admin' || $user->role =='super_admin')
                        <legend class="text-warning">Edit user</legend>
                        @else
                        <legend class="text-warning">Edit Profile</legend>
                        @endif
                        <input type="hidden" name="_method" value="PUT">
                        @csrf
                         <div class="form-row">
                            
                        <div class="form-group col">
                            <label for="">Name</label>
                            <input type="text" autofocus  name="name" value="{{old('name', $user->name)}}" class="form-control" >
                        </div>
                        
                        <div class="form-group col">
                            <label for="">Email</label>
                            <input type="email"   name="email" value="{{old('email', $user->email)}}" class="form-control" >
                        </div> 
                        {{-- <div class="form-group col">
                            <label for="">Hourly Rate</label>
                            <input type="number" step="0.1"  required  name="hourly_rate" value="{{old('hourly_rate', $user->hourly_rate)}}" class="form-control" >
                        </div>  --}}
                        </div>
                        <div class="form-row">
                            
                        <div class="form-group col">
                            <label>Role</label>
                            <select name="role" class="form-control" required="required">
                                @if(Auth::user()->role == 'admin')  
                                    <option @if(old('role', $user->role) == 'admin') selected @endif value="admin">Admin</option>
                                @elseif(Auth::user()->role == 'super_admin') 
                                    <option @if(old('role', $user->role) == 'admin') selected @endif value="admin">Admin</option>
                                    <option @if(old('role', $user->role) == 'super_admin') selected @endif value="super_admin">Super Admin</option>
                                @endif
                                <option @if(old('role', $user->role) == 'user') selected @endif value="user">User</option>
                            </select>
                        </div>

                          <div class="form-group col">
                            <label for="">Password</label>
                            <input type="password"   name="password" class="form-control is-invalid" >
                        </div> 
                          <div class="form-group col">
                            <label for="">Confirm Password</label>
                            <input type="password"   name="password_confirmation" class="form-control is-invalid" >
                        </div> 
                        </div>
                        <div class="alert alert-warning" role="alert">Enter password only if you want to change it </div>
                        <button type="submit" class="btn btn-warning">Save</button>
                    <a href="{{url('user')}}" class="btn btn-secondary">Cancel</a>
                    </form>
                    
                    @else

                    
                    <form action="{{url('user')}}" method="POST" role="form">
                        <legend class="text-primary">Add User</legend>
                        @csrf
                        <div class="form-row">
                            
                        <div class="form-group col">
                            <label for="">Name</label>
                            <input type="text" autofocus  name="name" value="{{old('name')}}" class="form-control" >
                        </div>
                        
                        <div class="form-group col">
                            <label for="">Email</label>
                            <input type="email"   name="email" value="{{old('email')}}" class="form-control" >
                        </div> 

                          {{-- <div class="form-group col">
                            <label for="">Hourly Rate</label>
                            <input type="number" step="0.1"  required  name="hourly_rate" value="{{old('hourly_rate')}}" class="form-control" >
                        </div>  --}}
                        </div>
                        <div class="form-row">
                            
                        <div class="form-group col">
                            <label>Role</label>
                            <select  name="role" class="form-control" required="required">
                                <option @if(old('role') == 'admin') selected @endif value="admin">Admin</option>
                                <option @if(old('role') == 'user') selected @endif value="user">User</option>
                                
                                @if(Auth::user()->role == 'super_admin')
                                <option @if(old('role') == 'super_admin') selected @endif value="super_admin">Super Admin</option>
                                @endif
                                
                            </select>
                        </div>

                          <div class="form-group col">
                            <label for="">Password</label>
                            <input type="password"   name="password" class="form-control" >
                        </div> 
                          <div class="form-group col">
                            <label for="">Confirm Password</label>
                            <input type="password"   name="password_confirmation" class="form-control" >
                        </div> 
                        </div>

                        <button type="submit" class="btn btn-primary">Create</button>
                    </form>

                    @endif
                    
                   
                    
               @if(Auth::user()->role == 'admin' || Auth::user()->role == 'super_admin')
                <table style="margin-top:40px;" class="table table-hover table-sm">
                    <thead>
                        <tr>
                            <th style="width:80px;">#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th style="width:250px;">Action</th>
                        </tr>

                    </thead>
                        <tbody>
                            <?php $i=1; ?>
                        @foreach($users as $user)
                            @if(Auth::user()->role == 'admin' && $user->role == 'super_admin')
                            
                            @else
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->role}}</td>
                                <td>
                                <a href="{{route('user.edit', $user->id)}}" class="btn btn-sm btn-outline-warning">Edit</a>
                                <form class="form-delete" method="post" action="{{route('user.destroy', $user->id)}}">
                                    @csrf
                                    {{method_field('DELETE')}}
                                    <button type="submit" class="btn btn-sm btn-outline-danger btn-delete">Delete</button>
                                </form>
                                </td>
                            </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                    @endif
                    </div>
                    
            </div>

@endsection