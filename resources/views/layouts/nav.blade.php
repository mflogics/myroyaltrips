<nav class="navbar navbar-expand-md navbar navbar-dark bg-dark shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
        <!-- <img style="max-width:90px;padding:0px;" src="{{url('/img/logo.png')}}" alt="logo"/> -->
            {{config('app.name', 'IJA')}}
        </a>


        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                @auth
                    <li><a class="nav-link" href="{{ url('/') }}">Dashboard</a>

                    @if(Auth::user()->role =='user')
                        <li><a class="nav-link text-warning" href="{{ url('trip') }}">Current Tripsheet</a></li>
                    @endif
                    <li><span class="nav-link">TripSheets:</span></li>
                    <li><a class="nav-link text-info" href="{{ url('tripsheets/submitted') }}">Submitted </a></li>
                    <li><a class="nav-link text-success" href="{{ url('tripsheets/approved') }}">Approved </a></li>
                    <li><a class="nav-link text-danger" href="{{ url('tripsheets/rejected') }}">Rejected </a></li>
                    <li><a class="nav-link text-warning" href="{{ url('tripsheets/missing') }}">Missing</a></li>

                    @if(Auth::user()->role =='super_admin')
                        <li><a class="nav-link text-warning" href="{{ url('tripsheets/deleted') }}">Deleted</a></li>

                    @endif





                    <li><a class="nav-link" href="{{ url('user') }}">
                            @if(Auth::user()->role == 'admin' || Auth::user()->role == 'super_admin')
                                Users
                            @else
                                Settings
                            @endif
                        </a></li>


                @endauth
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                @else




                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>