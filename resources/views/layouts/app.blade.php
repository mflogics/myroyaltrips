<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{asset('css/app.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/datetimepicker@latest/dist/DateTimePicker.min.css" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}" />
    <style type="text/css">
        .form-delete {
            display: inline;
        }
        .link-disable {
            pointer-events: none;
            opacity: 0.5;
        }
        .import-file-container {
            margin: 0 0 30px 0;
            padding: 20px 0 23px 0;
            background-color: #ededed;
        }
        .badge-purple {
            color: #fff;
            background-color: #4B0082	;
        }
    </style>
</head>

<body>
    <div id="app">

        @include('layouts.nav')

        <div class="container-fluid" style="margin:0;padding:0;">
            @yield('content-fluid')
        </div>
        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @endif @if ($errors->any())
                        <div class="alert alert-danger">
                            {{$errors->first()}}

                        </div>
                        @endif @yield('content')

                    </div>
                </div>
            </div>


        </main>




    </div>


    @yield('modal')

    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
 
    
{{-- <script src="https://cdn.jsdelivr.net/combine/npm/accounting-js@1.1.1,npm/vue@2.5.16,npm/vue-numeric@2.3.0"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>

{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.2.61/jspdf.min.js" type="text/javascript"></script> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/3.0.2/jspdf.plugin.autotable.min.js"></script> --}}
<script type="text/javascript">
  
</script>

    <script>
        window.user = {!! json_encode(Auth::user()) !!}
        window.currency_values = {!! json_encode([ 'USD'=> \Setting::get('USD'), 'GBP' => \Setting::get('GBP'), 'EUR' => 1 ]) !!}
    </script>


    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/datetimepicker@latest/dist/DateTimePicker.min.js"></script>

    <script type="text/javascript">


        $(function () {

            $('[data-toggle="tooltip"]').tooltip()


            $('.form-delete').on('submit', function (e) {
                if (!confirm("Are you sure you want to delete?")) {
                    e.preventDefault();
                }
            });

            //  $('.datepicker').datepicker();

            
           $("#dtBox").DateTimePicker({
            dateTimeFormat:'yyyy-MM-dd HH:mm'
           });
        });
    </script> 
    @yield('scripts')
</body>

</html>