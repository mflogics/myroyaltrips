@extends('layouts.app')

@section('content')

    <legend style="text-transform:uppercase;">{{$filter}} Tripsheets</legend>
    <div class="row">
        <div class="col-12">
            <div id="dtBox"></div>
            <form method="GET">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>From Date: </label>

                            <input type="text" autocomplete="off" name="from" data-field="date"
                                   data-format="{{env('DATE_FORMAT_JS')}}" value="{{$_GET['from'] or ''}}"
                                   class="form-control datetimepicker">

                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label> To Date: </label>
                            <input type="text" autocomplete="off" name="to" data-field="date"
                                   data-format="{{env('DATE_FORMAT_JS')}}" value="{{$_GET['to'] or ''}}"
                                   class="form-control datetimepicker">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        @if(Auth::user()->role != 'user')
                            <div class="form-group">
                                <label>Driver: </label>
                                <select class="form-control" name="driver_id">
                                    <option value="">All</option>
                                    @foreach ($drivers as $driver)
                                        <option @if(!empty($_GET['driver_id']) &&    $_GET['driver_id']==$driver->id) selected
                                                @endif value="{{$driver->id}}">{{$driver->name}} ({{$driver->email}})
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-3">
                        @if(Auth::user()->role != 'user')
                            <div class="form-group">
                                <label>Status: </label>
                                <select class="form-control" name="payment_status">
                                    <option @if(!empty($_GET['payment_status']) && $_GET['payment_status']=='all') selected
                                            @endif value="all">All
                                    </option>
                                    <option @if(!empty($_GET['payment_status']) && $_GET['payment_status']=='approved') selected
                                            @endif value="approved">Approved
                                    </option>
                                    <option @if(!empty($_GET['payment_status']) && $_GET['payment_status']=='rejected') selected
                                            @endif value="rejected">Rejected
                                    </option>
                                    <option @if(!empty($_GET['payment_status']) && $_GET['payment_status']=='submitted') selected
                                            @endif value="submitted">Submitted
                                    </option>
                                    <option @if(!empty($_GET['payment_status']) && $_GET['payment_status']=='paid') selected
                                            @endif value="paid">Paid
                                    </option>
                                    <option @if(!empty($_GET['payment_status']) && $_GET['payment_status']=='unpaid') selected
                                            @endif value="unpaid">Unpaid
                                    </option>
                                    <option @if(!empty($_GET['payment_status']) && $_GET['payment_status']=='deleted') selected
                                            @endif value="deleted">Deleted
                                    </option>
                                    <option @if(!empty($_GET['payment_status']) && $_GET['payment_status']=='missing') selected
                                            @endif value="missing">Missing
                                    </option>
                                </select>
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-3">
                        @if(Auth::user()->role != 'user')
                            <div class="form-group">
                                <label>By Reservation Date: </label>
                                <select class="form-control" name="order_by">
                                    <option @if(!empty($_GET['order_by']) && $_GET['order_by']=='asc') selected
                                            @endif value="asc">Ascending
                                    </option>
                                    <option @if(!empty($_GET['order_by']) && $_GET['order_by']=='desc') selected
                                            @endif value="desc">Descending
                                    </option>
                                </select>
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-3">
                        @if(Auth::user()->role != 'user')
                            <div class="form-group">
                                <label>By User: </label>
                                <select class="form-control" name="order_by_user">
                                    <option @if(!empty($_GET['order_by_user']) && $_GET['order_by_user']=='asc') selected
                                            @endif value="asc">Ascending
                                    </option>
                                    <option @if(!empty($_GET['Ascendingorder_by_user']) && $_GET['order_by_user']=='desc') selected
                                            @endif value="desc">Descending
                                    </option>
                                </select>
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-3" style="margin-top: 30px; margin-bottom: 10px;">
                        <button type="submit" class="btn btn-primary ml-2">Submit</button>
                        <a href="{{ url()->current() }}" class=" btn btn-default">Reset</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">

        <div class="col">

            <table class="table table-md alert-info">
                <tr>
                    <th colspan="100%">Summary</th>
                </tr>
                <tr class="col-lg-6">
                    <td>Submitted Results:</td>
                    <td>{{$submitted}}</td>
                    <td>Rejected Results:</td>
                    <td>{{$rejected}}</td>
                </tr>
                <tr>
                    <td>Missing Results:</td>
                    <td>{{$approved}}</td>
                    <td>Approved Results:</td>
                    <td>{{$missing}}</td>
                </tr>
                <tr>
                    <td>Deleted Results:</td>
                    <td>{{$deleted}}</td>
                    <td>Total Results:</td>
                    <td>{{$tripsheets->total()}}</td>
                </tr>
                <tr>
                    <td>Total Driver Owes:</td>
                    <td>${{$total_owes}}</td>
                </tr>
            </table>
        </div>
    </div>
    {{csrf_field()}}
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-success" style="display: none"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="row">
                @if(Auth::user()->role != 'user')
                    <div class="col-xs-12 col-lg-12 col-sm-12">
                        <form method="post" action="{{route('tripsheet.upload')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row import-file-container">
                                <div class="col-sm-12">
                                    <h4>Import Trips</h4>
                                </Div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Driver: </label>
                                        <select class="form-control" name="driver_id">
                                            <option value="">Select Driver</option>
                                            @foreach ($drivers as $driver)
                                                <option value="{{$driver->id}}">{{$driver->name}}
                                                    ({{$driver->email}})
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="label-upload">Select File</label>
                                        <div class="custom-control-upload">
                                            <input type="file" name="import_file" class="btn btn-secondary">
                                            <input type="submit" name="Submit" class="btn btn-success">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                @endif
            </div>
            <div class="row">
                @if(Auth::user()->role != 'user')
                    <div class="col-xs-12 col-lg-12 col-sm-12">
                        <form method="post" action="{{route('tripsheet.compare')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row import-file-container">
                                <div class="col-sm-12">
                                    <h4>Compare Imports</h4>
                                </Div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Driver: </label>
                                        <select class="form-control" name="driver_id">
                                            <option value="">Select Driver</option>
                                            @foreach ($drivers as $driver)
                                                <option value="{{$driver->id}}">{{$driver->name}}
                                                    ({{$driver->email}})
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    @if(Auth::user()->role != 'user')
                                        <div class="form-group">
                                            <label>Status: </label>
                                            <select class="form-control" name="status">
                                                <option @if(!empty($_GET['payment_status']) && $_GET['payment_status']=='all') selected
                                                        @endif value="all">All
                                                </option>
                                                <option @if(!empty($_GET['payment_status']) && $_GET['payment_status']=='approved') selected
                                                        @endif value="approved">Approved
                                                </option>
                                                <option @if(!empty($_GET['payment_status']) && $_GET['payment_status']=='rejected') selected
                                                        @endif value="rejected">Rejected
                                                </option>
                                                <option @if(!empty($_GET['payment_status']) && $_GET['payment_status']=='submitted') selected
                                                        @endif value="submitted">Submitted
                                                </option>
                                                <option @if(!empty($_GET['payment_status']) && $_GET['payment_status']=='paid') selected
                                                        @endif value="paid">Paid
                                                </option>
                                                <option @if(!empty($_GET['payment_status']) && $_GET['payment_status']=='unpaid') selected
                                                        @endif value="unpaid">Unpaid
                                                </option>
                                                <option @if(!empty($_GET['payment_status']) && $_GET['payment_status']=='deleted') selected
                                                        @endif value="deleted">Deleted
                                                </option>
                                                <option @if(!empty($_GET['payment_status']) && $_GET['payment_status']=='missing') selected
                                                        @endif value="missing">Missing
                                                </option>
                                            </select>
                                        </div>
                                    @endif
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="label-upload">Select File</label>
                                        <div class="custom-control-upload">
                                            <input type="file" name="import_file" class="btn btn-secondary">
                                            <input type="submit" name="Submit" class="btn btn-success">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @if($tripsheets->count()==0)
        <div class="alert text-center alert-secondary p-2" role="alert">

            <p class="m-0 p-0">No tripsheet yet</p>

        </div>
    @else
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="row">
                    @if(Auth::user()->role != 'user')
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                {{--                                <label>Update Bulk: </label>--}}
                                {{--                                <select class="form-control" name="bulk_option">--}}
                                {{--                                    <option value="all">Select All Records</option>--}}
                                {{--                                    <option value="listed_records">Select Records in List--}}
                                {{--                                    </option>--}}
                                {{--                                </select>--}}
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input select_all" id="customCheck1"
                                           name="selectAll"
                                           value="value">
                                    <label class="custom-control-label" for="customCheck1">Select
                                        All</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                {{--                                <label>Select Status: </label>--}}
                                <select class="form-control bulk_update_status" name="bulk_update_status">
                                    <option value="">None
                                    </option>
                                    <option value="approved">Approve
                                    </option>
                                    <option value="rejected">Reject
                                    </option>
                                    <option value="submitted">Submit
                                    </option>
                                    <option value="paid">Paid
                                    </option>
                                    <option value="unpaid">Unpaid
                                    </option>
                                    <option value="deleted">Delete
                                    </option>
                                    <option value="missing">Missing
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <input type="button" value="Submit" class="update_bulk_button btn btn-success">
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <input type="button" value="Close All" class="btn btn-primary float-right"
                       onclick="close_all({{$tripsheets->currentPage()}}, {{$tripsheets->perPage()}})">
                <input type="button" value="Open All" class="btn btn-warning float-right"
                       onclick="collapse_all({{$tripsheets->currentPage()}}, {{$tripsheets->perPage()}})">

                <a class="btn btn-success"
                   href="{{url()->current()}}?order_by={{empty($_GET['order_by']) || $_GET['order_by'] == 'desc' ? 'asc' : 'desc'}}&order_by_user={{empty($_GET['order_by_user'])  ? '' : $_GET['order_by_user']}}&from={{!empty($_GET['from']) ? $_GET['from'] : ''}}&to={{!empty($_GET['to']) ? $_GET['to'] : ''}}&driver_id={{!empty($_GET['driver_id']) ? $_GET['driver_id'] : ''}}&payment_status={{!empty($_GET['payment_status']) ? $_GET['payment_status'] : ''}}">{{empty($_GET['order_by']) || $_GET['order_by'] == 'desc' ? 'Asc by date' : 'Desc by date'}}</a>
                <a class="btn btn-success"
                   href="{{url()->current()}}?order_by={{empty($_GET['order_by'])  ? '' : $_GET['order_by']}}&order_by_user={{empty($_GET['order_by_user']) || $_GET['order_by_user'] == 'desc' ? 'asc' : 'desc'}}&from={{!empty($_GET['from']) ? $_GET['from'] : ''}}&to={{!empty($_GET['to']) ? $_GET['to'] : ''}}&driver_id={{!empty($_GET['driver_id']) ? $_GET['driver_id'] : ''}}&payment_status={{!empty($_GET['payment_status']) ? $_GET['payment_status'] : ''}}">{{empty($_GET['order_by_user']) || $_GET['order_by_user'] == 'desc' ? 'Asc by user' : 'Desc by user'}}</a>
            </div>
        </div>
    @endif

    @foreach($tripsheets as $row)


        <div class="card mb-2">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h5 class="card-title">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input tripsheet_id select_one"
                                       id="customCheck{{$row->id}}" name="select_rec[]"
                                       value="{{$row->id}}">
                                <label class="custom-control-label" for="customCheck{{$row->id}}"><span
                                            class="badge badge-warning">#{{$loop->index+1+ (($tripsheets->currentPage()-1)*$tripsheets->perPage())}}</span>
                                    <small>Driver:</small> {{$row->user->name}}</label>
                            </div>
                        </h5>
                    </div>
                    <div class="col-sm-12">
                        <span class="float-right">
                        @if(($is_admin || $is_super_admin))
                                <label for="payment_status">Paid</label>
                                <input type="checkbox" data-trip_sheet_id="{{$row->id}}"
                                       {{$row->payment_status == 'paid' ? 'checked' : ''}} class="form-check-inline custom-checkbox"
                                       id="payment_status">
                            @endif
                            @if(($is_admin || $is_super_admin) && $row->status=='submitted')

                                <a href="{{url("tripsheet/{$row->id}/status/approved")}}"
                                   class="btn btn-approve btn-sm btn-outline-success {{$row->payment_status == 'paid' ? 'link-disable' : ''}}">Approve</a>
                                <a href="{{url("tripsheet/{$row->id}/status/rejected")}}"
                                   class="btn btn-reject btn-sm btn-outline-danger {{$row->payment_status == 'paid' ? 'link-disable' : ''}}">Reject</a>
                                <span class="badge badge-primary">{{$row->status}}</span> on:
                                <small>{{$row->updated_at}}</small>
                            @else

                                @if($row->status == 'rejected' && Auth::user()->role == 'user')
                                    <a href="{{url("tripsheet/{$row->id}/status/submitted")}}"
                                       class="btn btn-sm btn-outline-success">Re Submit</a>
                                @endif
                                @if($row->status == 'rejected' && Auth::user()->role != 'user')
                                    <a href="{{url("tripsheet/{$row->id}/status/approved")}}"
                                       class="btn btn-sm btn-approve btn-outline-success {{$row->payment_status == 'paid' ? 'link-disable' : ''}}">Approve</a>
                                @else

                                    {{--                                    <a href="{{url("tripsheet/{$row->id}/status/rejected")}}"--}}
                                    {{--                                       class="btn btn-sm btn-reject btn-outline-danger {{$row->payment_status == 'paid' ? 'link-disable' : ''}}">Reject</a>--}}
                                @endif


                                @if($row->status == 'missing')
                                    <span class="badge badge-purple">{{ucwords($row->status)}}</span>
                                    on: <small>{{$row->updated_at}}</small>
                                @else
                                    <span class="badge {{$row->status == 'approved'? 'badge-success': ($row->status == 'rejected'? 'badge-danger': 'badge-primary') }}">{{$row->status}}</span>
                                    on: <small>{{$row->updated_at}}</small>
                                @endif
                            @endif

                            @if(($is_admin || $is_super_admin) && $row->status!='submitted')

                                @if($is_super_admin && $row->status =='deleted')
                                    <a href="{{url("tripsheet/{$row->id}/status/approved")}}"
                                       class="btn btn-sm btn-approve btn-outline-success">Approve</a>
                                    <a href="{{url("tripsheet/{$row->id}/status/rejected")}}"
                                       class="btn btn-sm btn-reject btn-outline-danger">Reject</a>

                                    <form class="form-delete" method="post" action="{{url('tripsheet', $row->id)}}">
                                    @csrf {{method_field('DELETE')}}
                                    <button type="submit"
                                            class="btn btn-sm btn-danger btn-delete">Permanently Delete</button>
                                </form>
                                @else
                                    <a href="{{url("tripsheet/{$row->id}/status/deleted")}}"
                                       class="btn btn-sm btn-trip-delete btn-outline-danger btn-delete {{$row->payment_status == 'paid' ? 'link-disable' : ''}}">Delete</a>
                                @endif

                            @endif
                    </span>
                    </div>

                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        Trip Sheet Date: {{$row->reservation_date}}
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <button class="btn btn-default float-right btn-sm"
                                id="sd_btn_{{$loop->index+1+ (($tripsheets->currentPage()-1)*$tripsheets->perPage())}}"
                                type="button" data-toggle="collapse" data-target=".collapsable_{{$row->id}}">
                            Show Detail
                        </button>
                    </div>
                </div>
                <div class="collapse collapsable_{{$row->id}}"
                     id="div_a_{{$loop->index+1+ (($tripsheets->currentPage()-1)*$tripsheets->perPage())}}"
                     name="{{$row->id}}">
                    <p class="card-text collapsecollapsable_{{$row->id}}">

                    <div class="form-row">
                        <div class="col">
                            <strong>Mileage From:</strong> {{$row->mileage_start}}<br>
                            <strong>Mileage To: </strong>{{$row->mileage_end}}

                        </div>
                        <div class="col text-right">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><label
                                    style="font-size:smaller; color:silver;">Engine
                                Oil</label><br>{{$row->engine_oil}}</div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><label
                                    style="font-size:smaller; color:silver;">Transmission
                                Fluid</label><br>{{$row->transmission_fluid}}</div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><label
                                    style="font-size:smaller; color:silver;">Coolant</label><br>{{$row->coolant}}</div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><label
                                    style="font-size:smaller; color:silver;">Note</label><br>{{$row->note}}</div>

                        @if(!(($is_admin || $is_super_admin) && $row->status=='submitted'))
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><label
                                        style="font-size:smaller; color:silver;">Admin
                                    note</label><br>
                                <form method="post" action="{{route('tripsheet.adminnoteupdate')}}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="trip_sheet_id" value="{{$row->id}}">
                                    <input type="text" class="form-control mb-2" name="admin_note"
                                           value="{{$row->admin_note}}">
                                    <input type="submit" style="float: right"
                                           class="btn btn-sm btn-approve btn-outline-success"
                                           name="submit" value="Save">
                                </form>

                            </div>
                        @endif
                    </div>
                    @if(($is_admin || $is_super_admin) && $row->status=='submitted')
                        <hr>
                        <div class="form-row">
                            <form method="POST" action="{{route('update.tripsheet')}}" class="form-inline">
                                {{csrf_field()}}
                                <input type="hidden" name="id" value="{{$row->id}}"/>
                                <div class="col">
                                    <label style="font-size:smaller; color:#28a745;"> Approve</label> <input
                                            value="approved" type="radio" name="status" checked/>
                                </div>
                                <div class="col">
                                    <label style="font-size:smaller; color:#dc3545;"> Reject</label> <input
                                            value="rejected" class="" type="radio" name="status"/>
                                </div>
                                <div class="col"><label style="font-size:smaller; color:silver;">Admin Note</label>
                                </div>
                                <div class="col-3">
                                    <textarea name="admin_note" class="form-control"
                                              pattern="[^,]+">{{$row->admin_note}}</textarea>
                                </div>
                                <div class="col"><label style="font-size:smaller; color:silver;">Miscelleneous
                                        Charge/Credit (eg -5 for Charges[$5])</label></div>
                                <div class="col-3">
                                    <input name="misc"
                                           id="misc_{{$loop->index+1+ (($tripsheets->currentPage()-1)*$tripsheets->perPage())}}"
                                           class="form-control" pattern="[^,]+" type="number" value="{{$row->misc}}"
                                           onclick="my_function('misc_{{$loop->index+1+ (($tripsheets->currentPage()-1)*$tripsheets->perPage())}}', 'lbl_{{$loop->index+1+ (($tripsheets->currentPage()-1)*$tripsheets->perPage())}}', 'ttl_{{$loop->index+1+ (($tripsheets->currentPage()-1)*$tripsheets->perPage())}}')"/>

                                </div>
                                <div class="col">
                                    <input type="submit" class="btn btn-success" value="Submit"/>
                                </div>
                            </form>
                        </div>
                    @endif
                </div>

            </div>

            <div class="collapse collapsable_{{$row->id}}"
                 id="div_b_{{$loop->index+1+ (($tripsheets->currentPage()-1)*$tripsheets->perPage())}}">
                <input id="inp_{{$loop->index+1+ (($tripsheets->currentPage()-1)*$tripsheets->perPage())}}"
                       value="{{$row->id}}" type="hidden"></input>
                <div class="table-responsive">

                    <table class="table">
                        <thead>
                        <tr>
                            <th class="alert-warning" colspan="100%">Trips ({{$row->trips->count()}})
                                @if($row->status == 'rejected' && Auth::user()->role == 'user')
                                    <a href="{{url("trip/?sheet={$row->id}")}}" class="btn btn-sm btn-outline-success">Add
                                        More</a>
                                @endif

                            </th>
                        </tr>
                        <tr>
                            <th>Customer</th>
                            <th>When</th>
                            <th>From/To</th>
                            <th>Fees Cash</th>
                            <th>Fees Credit</th>
                            <th>Fare Cash</th>
                            <th>Fare CC/Voucher / V#</th>
                            <th>CC/V Tips</th>
                            <th>Image</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($row->trips as $t)
                            <tr>
                                <td>{{$t->customer}}</td>
                                <td>{{$t->when}}</td>
                                <td>{{$t->from}}/{{$t->to}}</td>
                                <td>{{$t->fees_cash}}</td>
                                <td>{{$t->fees_credit}}</td>
                                <td>{{$t->fare_cash}}</td>
                                <td>{{$t->voucher_amount}}/{{$t->voucher_number}}</td>
                                <td>{{$t->tip}}</td>
                                {{-- <td>@if($t->image) <a href="#" data-toggle="modal" data-target="#modalPreview" class="show_image" > <img src="{{$t->image}}" style="height:32px;" class="rounded"></a>  @endif </td> --}}
                                <td>@if($t->image) <a href="#" data-toggle="modal" data-target="#modalPreview"
                                                      data-trip-id="{{$t->id}}" class="show_image">preview</a>  @endif
                                </td>
                                <td>
                                    @if($row->status != 'approved')
                                        <span class="float-right">
                                <a href="{{route('trip.edit', $t->id)}}" class="btn btn-sm btn-outline-warning">Edit</a>
                                    <form class="form-delete" method="post" action="{{route('trip.destroy', $t->id)}}">
                                        @csrf {{method_field('DELETE')}}
                                        <button type="submit"
                                                class="btn btn-sm btn-outline-danger btn-delete">Delete</button>
                                    </form>
                                </span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <?php $stats = \App\Trip::getStats($row->trips); ?>
            <div class="card-footer">
                <input type="hidden" value="{{$stats['DRIVER_OWES']}}"
                       id="ttl_{{$loop->index+1+ (($tripsheets->currentPage()-1)*$tripsheets->perPage())}}">
                <div class="col"><label style="font-size:smaller;color:blue;">Driver Owes</label> <label
                            id="lbl_{{$loop->index+1+ (($tripsheets->currentPage()-1)*$tripsheets->perPage())}}">${{$stats['DRIVER_OWES'] - $row->misc }}</label>
                </div>
                <div class="form-row collapse  collapsable_{{$row->id}}"
                     id="div_cf_{{$loop->index+1+ (($tripsheets->currentPage()-1)*$tripsheets->perPage())}}">
                    <div class="colx col-sm-3"><label style="font-size:smaller;color:gray;">Gross
                            Sale</label><br>${{$stats['GROSS_SALE']}}</div>
                    <div class="colx col-sm-3"><label style="font-size:smaller;color:gray;">Total
                            Fees</label><br>${{$stats['TOTAL_FEES']}}</div>
                    <div class="colx col-sm-3"><label style="font-size:smaller;color:gray;">Total Cash &amp; Credit
                            Fare</label><br>${{$stats['TOTAL_CASH_CREDIT_FARE']}}</div>
                    <div class="colx col-sm-3"><label style="font-size:smaller;color:gray;">Driver
                            Subtotal</label><br>${{$stats['DRIVER_OWES']}}</div>
                    <div class="colx col-sm-3"><label style="font-size:smaller;color:gray;">Gross to
                            Royal</label><br>${{$stats['GROSS_TO_ROYAL']}}</div>
                    <div class="colx col-sm-3"><label style="font-size:smaller;color:gray;">Total Credit
                            Fares/Tips</label><br>${{$stats['TOTAL_CREDIT_FARE_TRIPS']}}</div>
                    <div class="colx col-sm-3"><label
                                style="font-size:smaller;color:gray;">60%</label><br>${{$stats['SIXTY_PERCENT']}}</div>
                    <div class="colx col-sm-3"><label style="font-size:smaller;color:gray;">Miscelleneous
                            Charge/Credit</label><br>${{$row->misc}}</div>
                </div>
            </div>
        </div>


    @endforeach

    @if(!empty($pagi))
        {{$tripsheets->appends($pagi)->links()}}
    @else
        {{$tripsheets->links()}}
    @endif





@endsection


@section('modal')

    <!-- Modal -->
    <div class="modal fade" id="modalPreview" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
         aria-hidden="true">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Image Preview</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-bod0y p-1">
                    <div id="loading_indicator" class="spinner-border" role="status">
                        <span>Loading...</span>
                    </div>
                    <a id="pdfload" href="" target="_blank" class="btn btn-success" style="hidden"> Open Pdf </a>
                    <img src="" alt="" id="modal-image" class="rounded img-fluid">
                    <iframe id="pdfloader" src=""></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script type="text/javascript">
        $(document).ready(function () {
            $(".select_all").click(function () {
                $(".tripsheet_id").prop('checked', $(this).prop('checked'));
            });
            $('.update_bulk_button').click(function () {
                var select_tripsheet_ids = [];
                var status_value = $('.bulk_update_status').val();
                $('.select_one:checked').each(function (i) {
                    select_tripsheet_ids[i] = $(this).val();
                });
                var dataObj = {
                    ids: select_tripsheet_ids,
                    status_value: status_value,
                };
                // console.log(select_tripsheet_ids);
                if (status_value == "") {
                    alert('Please select any status');
                } else if (select_tripsheet_ids == "") {
                    alert('Please select any tripsheet');
                } else {
                    var request = $.ajax({
                        url: '{{route('bulk.update.tripsheet')}}',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: "POST",
                        data: dataObj,
                        dataType: "json"
                    });
                    request.done(function (response) {
                        window.location.reload(true);
                    });
                    //
                    request.fail(function (jqXHR, textStatus) {
                        alert('Something went wrong');
                    });
                    // console.log(status_value);
                }

            });
            $('body').on('click', '#payment_status', function () {
                var that = $(this);
                var trip_sheet_id = that.data('trip_sheet_id');
                var payment_status = '';
                if (that.prop('checked') == true) {
                    //do something
                    payment_status = 'paid';

                } else {
                    payment_status = 'unpaid';
                }
                var request = $.ajax({
                    url: '{{route('update.tripsheet.payment')}}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    data: {
                        trip_sheet_id: trip_sheet_id,
                        payment_status: payment_status
                    },
                    dataType: "json"
                });

                request.done(function (response) {
                    $('.alert-success').show().text(response[0].message);
                    if (payment_status == 'paid') {
                        that.closest('.float-right').find('.btn-reject').addClass('link-disable');
                        that.closest('.float-right').find('.btn-approve').addClass('link-disable');
                        that.closest('.float-right').find('.btn-trip-delete').addClass('link-disable');
                    } else {
                        that.closest('.float-right').find('.btn-reject').removeClass('link-disable');
                        that.closest('.float-right').find('.btn-approve').removeClass('link-disable');
                        that.closest('.float-right').find('.btn-trip-delete').removeClass('link-disable');
                    }
                    $(window).scrollTop(0);
                });

                request.fail(function (jqXHR, textStatus) {
                    alert('Something went wrong');
                });
            });
        });
        $(function () {
            $('.show_image').on('click', function () {
                var tripId = $(this).data('trip-id');
                // var src = $(this).find('img').attr('src');

                $('#modal-image').hide();
                $('#loading_indicator').show();
                $('#pdfload').hide();
                $('#pdfloader').hide();
                $.get("{{url('api/trip_image/')}}/" + tripId, function (data) {
                    if (data.match('data:image')) {
                        $('#modal-image').attr('src', data);
                        $('#modal-image').show();
                    } else if (data.match('data:application/pdf')) {
                        $('#pdfloader').attr('src', data);
                        $('#pdfloader').show();
                    } else if (data.match('pdf')) {
                        $('#pdfload').attr('href', '/' + data);
                        $('#pdfload').show();
                    } else {
                        $('#modal-image').attr('src', '/' + data);
                        $('#modal-image').show();
                    }

                    $('#loading_indicator').hide();
                    //$('#modal-image').show();
                })

            })
        });

        function my_function(btn_id, lbl_id, ttl_id) {
            var val = document.getElementById(ttl_id).value - document.getElementById(btn_id).value;
            document.getElementById(lbl_id).innerHTML = "$" + val;
        }

        function close_all(current_page, per_page) {

            var start_record = (per_page * (current_page - 1)) + 1;
            var end_record = start_record + per_page - 1;

            for (var my_record = start_record; my_record <= end_record; my_record++) {
                var btn_id = "sd_btn_" + my_record;

                var div_a_id = "div_a_" + my_record;
                var div_b_id = "div_b_" + my_record;
                var div_cf_id = "div_cf_" + my_record;

                var inp_id = "inp_" + my_record
                var data_tgt = document.getElementById(inp_id).value;


                var div_msg = "collapsable_" + data_tgt + " collapse";
                var div_cf_msg = "form-row collapsable_" + data_tgt + " collapse";

                document.getElementById(btn_id).setAttribute("class", "btn btn-default float-right btn-sm collapsed");
                document.getElementById(btn_id).setAttribute("aria-expanded", "false");


                document.getElementById(div_a_id).setAttribute("class", div_msg);
                document.getElementById(div_b_id).setAttribute("class", div_msg);
                document.getElementById(div_cf_id).setAttribute("class", div_cf_msg);

            }
        }


        function collapse_all(current_page, per_page) {
            var start_record = (per_page * (current_page - 1)) + 1;
            var end_record = start_record + per_page - 1;
            for (var my_record = start_record; my_record <= end_record; my_record++) {
                var btn_id = "sd_btn_" + my_record;

                var div_a_id = "div_a_" + my_record;
                var div_b_id = "div_b_" + my_record;
                var div_cf_id = "div_cf_" + my_record;
                var inp_id = "inp_" + my_record
                var data_tgt = document.getElementById(inp_id).value;


                var div_msg = "collapsable_" + data_tgt + " collapse show";
                var div_cf_msg = "form-row collapsable_" + data_tgt + " collapse show";


                document.getElementById(btn_id).setAttribute("class", "btn btn-default float-right btn-sm");
                document.getElementById(btn_id).setAttribute("aria-expanded", "true");


                document.getElementById(div_a_id).setAttribute("class", div_msg);
                document.getElementById(div_b_id).setAttribute("class", div_msg);
                document.getElementById(div_cf_id).setAttribute("class", div_cf_msg);

            }
        }
    </script>

@endsection
