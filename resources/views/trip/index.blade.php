@extends('layouts.app') 
@section('content')

<div class="card mb-2 item">
    <div class="card-body">

        @if(isset($trip))
        <h4 class="card-title">Edit Trip</h4>

        <form action="{{route('trip.update', $trip->id)}}" method="POST" role="form" enctype="multipart/form-data">

            <input type="hidden" name="_method" value="PUT"> @csrf
            <div class="form-row">

                <div class="form-group col-md-3 col-sm-6">
                    <label>Customer Name</label>
                    <input type="text" autocomplete="off" autofocus name="customer" value="{{old('customer', $trip->customer)}}" class="form-control">
                </div>
                <div class="form-group col-md-3 col-sm-6">
                    <label>From</label>
                    <input type="text" autocomplete="off" name="from" value="{{old('from', $trip->from)}}" class="form-control">
                </div>
                <div class="form-group col-md-3 col-sm-6">
                    <label>To</label>
                    <input type="text" autocomplete="off" name="to" value="{{old('to', $trip->to)}}" class="form-control">
                </div>
                <div class="form-group col-md-3 col-sm-6">
                    <label>When</label>
                    <input type="hidden" name="when" value="{{old('when', $trip->when)}}" />
                    <input type="text" autocomplete="off" data-field="date" data-format="{{env('DATE_FORMAT_JS')}}" value="{{old('when', $trip->when)}}"
                        class="form-control datetimepicker" disabled>
                </div>
            </div>
            <div class="form-row">

                <div class="form-group col-md-2 col-sm-6">
                    <label>Fees Cash</label>
                    <input type="number" step="0.01" name="fees_cash" value="{{old('fees_cash', $trip->fees_cash)}}" class="form-control">
                </div>
                <div class="form-group col-md-2 col-sm-6">
                    <label>Fees Credit</label>
                    <input type="number" step="0.01" name="fees_credit" value="{{old('fees_credit', $trip->fees_credit)}}" class="form-control">
                </div>
                <div class="form-group col-md-2 col-sm-6">
                    <label>Fare Cash</label>
                    <input type="number" step="0.01" name="fare_cash" value="{{old('fare_cash', $trip->fare_cash)}}" class="form-control">
                </div>
                <div class="form-group col-md-2 col-sm-6">
                    <label>Fare CC/Voucher</label>
                    <input type="number" step="0.01" name="voucher_amount" value="{{old('voucher_amount', $trip->voucher_amount)}}" class="form-control">
                </div>
                <div class="form-group col-md-2 col-sm-6">
                    <label>Voucher #</label>
                    <input type="text" autocomplete="off" name="voucher_number" value="{{old('voucher_number', $trip->voucher_number)}}" class="form-control">
                </div>
                <div class="form-group col-md-2 col-sm-6">
                    <label>CCV Tip </label>
                    <input type="number" step="0.01" name="tip" value="{{old('tip', $trip->tip)}}" class="form-control">
                </div>

            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label>Attach Image/pdf</label>

                    <input type="file" accept='image/*,application/pdf' class="form-control-file" name="imm" id="file">

                </div>

                <div class="col-md-6 border">
                    <label>Preview</label>
                    <div id="camera_result">
                        @if(isset($trip->image) && strpos($trip->image,'pdf') !== false)
                            <a target="_blank" href="{{asset($trip->image)}}" class="btn btn-success">View Pdf</a>
                        @else
                        <img class="img-fluid rounded" src="{{old('image', asset($trip->image))}}" />
                        @endif
                    </div>
                </div>
            </div>

            {{--
            <div class="form-row">


                <button type="button" id="attach_photo" class="btn btn-warning m-3">Attach Photo</button>
            </div>

            <div id="camera_stuff" class="form-row">
                <div class="col">
                    <div id="camera_preview"></div>
                    <div id="camera_result">
                        <img class="image thumbnail" src="{{old('image', $trip->image)}}" />
                    </div>

                    <input type="button" class="btn btn-default m-3" id="click_pic" value="Capture">
                </div>

            </div> --}}
            <input type="hidden" name="image" value="{{old('image', $trip->image)}}" class="image-tag">


            <button type="submit" class="btn btn-warning">Save</button>
            <a href="{{url('trip')}}" class="btn btn-secondary">Cancel</a>
        </form>

        @else


        <form action="{{url('trip')}}" method="POST" role="form" enctype="multipart/form-data">
            <h4 class="card-title">Add Trip</h4>
            @csrf
            <div class="form-row">
                @if(!empty($sheet))
                    <input type="hidden" name="trip_sheet_id" value="{{$sheet}}" />
                @endif
                <div class="form-group col-md-3">
                    <label>Customer Name</label>
                    <input type="text" autocomplete="off" autofocus name="customer" value="{{old('customer')}}" class="form-control">
                </div>
                <div class="form-group col-md-3">
                    <label>From</label>
                    <input type="text" autocomplete="off" name="from" value="{{old('from')}}" class="form-control">
                </div>
                <div class="form-group col-md-3">
                    <label>To</label>
                    <input type="text" autocomplete="off" name="to" value="{{old('to')}}" class="form-control">
                </div>

                <div class="form-group col-md-3">
                    <label>When</label>
                    @if(empty($sheetdate))
                    <input type="text" autocomplete="off" name="when" data-field="date" data-format="{{env('DATE_FORMAT_JS')}}" value="{{old('when')}}"
                        class="form-control datetimepicker">
                    @else
                    <input type="hidden" name="when" value="{{$sheetdate}}" >
                        <input type="text" autocomplete="off" data-field="date" data-format="{{env('DATE_FORMAT_JS')}}" value="{{$sheetdate}}"
                                    class="form-control" disabled>
                    @endif

                    
                </div>

            </div>
            <div class="form-row">

                <div class="form-group col-md-2 col-sm-6">
                    <label>Fees Cash</label>
                    <input type="number" step="0.01" name="fees_cash" value="{{old('fees_cash', 0)}}" class="form-control">
                </div>
                <div class="form-group col-md-2 col-sm-6">
                    <label>Fees Credit</label>
                    <input type="number" step="0.01" name="fees_credit" value="{{old('fees_credit', 0)}}" class="form-control">
                </div>
                <div class="form-group col-md-2 col-sm-6">
                    <label>Fare Cash</label>
                    <input type="number" step="0.01" name="fare_cash" value="{{old('fare_cash', 0)}}" class="form-control">
                </div>
                <div class="form-group col-md-2 col-sm-6">
                    <label>Fare CC/Voucher</label>
                    <input type="number" step="0.01" name="voucher_amount" value="{{old('voucher_amount', 0)}}" class="form-control">
                </div>
                <div class="form-group col-md-2 col-sm-6">
                    <label>Voucher #</label>
                    <input type="text" autocomplete="off" name="voucher_number" value="{{old('voucher_number')}}" class="form-control">
                </div>
                <div class="form-group col-md-2 col-sm-6">
                    <label>CCV Tip </label>
                    <input type="number" step="0.01" name="tip" value="{{old('tip', 0)}}" class="form-control">
                </div>

            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label>Attach Image/Pdf</label>

                    <input type="file" accept='image/*,application/pdf' class="form-control-file" name="imm" id="file">

                </div>

                <div class="col-md-6 border">
                    <label>Preview</label>
                    <div id="camera_result">
                        <img class="img-fluid rounded" src="{{old('image')}}" />
                    </div>
                </div>
            </div>

            {{--
            <div id="camera_stuff" class="form-row">
                <button type="button" id="attach_photo" class="btn btn-warning m-3">Attach Photo</button>
                <div class="col">
                    <div id="camera_preview"></div>


                    <input type="button" class="btn btn-default m-3" id="click_pic" value="Capture">
                </div>

            </div> --}}
            <input type="hidden" name="image" value="{{old('image')}}" class="image-tag">

            <button type="submit" class="btn btn-primary">Add Trip</button>
        </form>

        @endif

        <div id="dtBox"></div>



    </div>
</div>



<div class="row">
    <div class="col-12">


        @if($trips->count()>0)
        <button type="button" class="btn btn-danger float-right mb-3" data-toggle="collapse" data-target="#tripsheet_form">
                Submit Trip Sheet
            </button> @endif
    </div>
</div>

<div class="row">
    <div class="col-12">

        <div id="tripsheet_form" class="card shadow-lg mb-3 collapse border-danger">
            <div class="card-body">
                <h3 class="card-title text-danger">Submit Trip Sheet</h3>
                @if(isset($tripsheet))
                <form action="{{url('update_tripsheet/'+$tripsheet->id)}}" method="POST">
                    <input type="hidden" name="_method" value="PUT"> @else
                    <form action="{{url('submit_tripsheet')}}" method="POST">
                        @endif @csrf
                        <div class="row">
                            <div class="form-group col-md-3">
                                <label>Car Detail</label>
                                <input autocomplete="off" type="text" class="form-control" required name="car" value="{{old('car', isset($tripsheet)? $tripsheet->car: '')}}">
                            </div>
                            <div class="form-group col-md-3">
                                <label>Mileage Start</label>
                                <input autocomplete="off" type="number" step="0.01" class="form-control" required name="mileage_start" value="{{old('mileage_start', isset($tripsheet)? $tripsheet->mileage_start: '')}}">
                            </div>
                            <div class="form-group col-md-3">
                                <label>Mileage End</label>
                                <input autocomplete="off" type="number" step="0.01" class="form-control" required name="mileage_end" value="{{old('mileage_end', isset($tripsheet)? $tripsheet->mileage_end: '')}}">
                            </div>
                            <div class="form-group col-md-3">
                                <label>Engine Oil</label>
                                <input autocomplete="off" type="text" class="form-control" name="engine_oil" value="{{old('engine_oil', isset($tripsheet)? $tripsheet->engine_oil: '')}}">
                            </div>
                            <div class="form-group col-md-3">
                                <label>Transmission Fluid</label>
                                <input autocomplete="off" type="text" class="form-control" name="transmission_fluid" value="{{old('transmission_fluid', isset($tripsheet)? $tripsheet->transmission_fluid: '')}}">
                            </div>
                            <div class="form-group col-md-3">
                                <label>Coolant</label>
                                <input autocomplete="off" type="text" class="form-control" name="coolant" value="{{old('coolant', isset($tripsheet)? $tripsheet->coolant: '')}}">
                            </div>
                            <div class="form-group col-md-3">
                                <label>Trip Sheet Date</label>
                                <input type="hidden" name="reservation_date" value="{{isset($tripsheet)? $tripsheet->reservation_date:$sheetdate}}" >
                                <input type="text" autocomplete="off" data-field="date" data-format="{{env('DATE_FORMAT_JS')}}" value="{{isset($tripsheet)? $tripsheet->reservation_date:$sheetdate}}"
                                    class="form-control" disabled>
                            </div>

                            <div class="form-group col-md-12">
                                <label>Note/Remarks</label>
                                <textarea class="form-control" name="note" pattern="[^,]+" value="{{old('note', isset($tripsheet)? $tripsheet->note: '')}}"></textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-danger">{{isset($tripsheet)?'Update': 'Submit'}}</button>
                    </form>
            </div>
        </div>
    </div>
</div>


@if(!isset($trip) || $trip->trip_sheet_id == null)
<legend>
    Trips

</legend>

@if($trips->count()==0)
<div class="alert text-center alert-secondary p-2" role="alert">

    <p class="m-0 p-0">No trip found</p>

</div>
@endif @foreach($trips as $row)

<div class="card mb-2 {{isset($trip) && $row->id == $trip->id ? 'border-warning' : '' }}">
    <div class="card-body">
        <h5 class="card-title"><span class="badge badge-warning">#{{$loop->index+1}}</span> {{$row->customer}}

            @if($row->image) <a data-image="{{asset($row->image)}}" href="#" data-toggle="modal" data-target="#modelId" class="show_image"> Preview</a> @endif 
    
            <span class="float-right">
            <a href="{{route('trip.edit', $row->id)}}" class="btn btn-sm btn-outline-warning">Edit</a>
                            <form class="form-delete" method="post" action="{{route('trip.destroy', $row->id)}}">
                                @csrf {{method_field('DELETE')}}
                                <button type="submit" class="btn btn-sm btn-outline-danger btn-delete">Delete</button>
                            </form>
        </span>
        </h5>
        <p class="card-text">
            <div class="form-row">
                <div class="col">
                    <strong>From:</strong> {{$row->from}}<br>
                    <strong>To: </strong>{{$row->to}}

                </div>
                <div class="col">
                              </div>
                <div class="col text-right">
                    {{$row->when}}
                </div>
            </div>
            <div class="form-row">
                <div class="col"><label style="font-size:smaller; color:silver;">Gross Sale</label><br>$ {{number_format($row->gross_sale,2)}}</div>
                <div class="col"><label style="font-size:smaller; color:silver;">Fees Cash</label><br>$ {{number_format($row->fees_cash,2)}}</div>
                <div class="col"><label style="font-size:smaller; color:silver;">Fees Credit</label><br>$ {{number_format($row->fees_credit,2)}}</div>
                <div class="col"><label style="font-size:smaller; color:silver;">Fare Cash</label><br>$ {{number_format($row->fare_cash,2)}}</div>
                <div class="col"><label style="font-size:smaller; color:silver;">Voucher $ [#]</label><br>$ {{number_format($row->voucher_amount,2)}}
                    [{{$row->voucher_number}}]</div>
                <div class="col"><label style="font-size:smaller; color:silver;">Tip</label><br>$ {{number_format($row->tip,2)}}</div>
            </div>
             
        </p>
    </div>

</div>
@endforeach @endif
@endsection
 {{-- 
@section('modal')


<form action="{{url('submit_tripsheet')}}" method="POST">

    @csrf
    <!-- Modal -->
    <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Submit Trip Sheet</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">


                    <div class="form-group">
                        <label>Car Detail</label>
                        <input autocomplete="off" type="text" class="form-control" required name="car">
                    </div>
                    <div class="form-group">
                        <label>Mileage Start</label>
                        <input autocomplete="off" type="number" step="0.01" class="form-control" required name="mileage_start">
                    </div>
                    <div class="form-group">
                        <label>Mileage End</label>
                        <input autocomplete="off" type="number" step="0.01" class="form-control" required name="mileage_end">
                    </div>
                    <div class="form-group">
                        <label>Engine Oil</label>
                        <input autocomplete="off" type="text" class="form-control" name="engine_oil">
                    </div>
                    <div class="form-group">
                        <label>Transmission Fluid</label>
                        <input autocomplete="off" type="text" class="form-control" name="transmission_fluid">
                    </div>
                    <div class="form-group">
                        <label>Coolant</label>
                        <input autocomplete="off" type="text" class="form-control" name="coolant">
                    </div>
                    <div class="form-group">
                        <label>Note/Remarks</label>
                        <textarea class="form-control" pattern="[^,]+" name="note"></textarea>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
 --}} 
@section('modal')

<!-- Modal -->
<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Image Preview</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-bod0y p-1">
                <img src="" alt="" id="modal-image" class="rounded img-fluid">
                <iframe id="pdfloader" src=""></iframe>
                <a id="pdfload" href="" target="_blank" class="btn btn-success" style="hidden" > Open Pdf </a>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
@endsection
 
@section('scripts')




<!-- Configure a few settings and attach camera -->
<script type="text/javascript">
    Webcam.set({
        width: 490,
        height: 390,
        image_format: 'jpeg',
        jpeg_quality: 90
    });

    $(document).ready(function(){ 

        $('.show_image').on('click', function(){
            var src = $(this).data('image');
            if(src.match('.pdf')){
                $('#pdfload').attr('href', src);
                $('#pdfload').show();
                $('#modal-image').hide();
                $('#modal-image').attr('src', '');
            }else if(data.match('data:application/pdf')){
                    $('#pdfloader').attr('src',  data);
                    $('#pdfloader').show();
                    $('#modal-image').hide();
                    $('#pdfload').hide();
            }else{
                $('#modal-image').attr('src',  src);
                $('#modal-image').show();
                $('#pdfload').hide();
            }
        })



        
function readFile() {
  if (this.files && this.files[0]) {
    
    var FR= new FileReader();
    
    FR.addEventListener("load", function(e) {
      $("#camera_result img").attr('src',  e.target.result);
      $(".image-tag").val(e.target.result);
      //document.getElementById("b64").innerHTML = e.target.result;
    }); 
    
    FR.readAsDataURL( this.files[0] );
  }
  
}

//$('#file').on('change', readFile);




        @if(!empty($trip))
            $('#camera_stuff').show();
            $('#camera_result').show();
        @else
             $('#camera_stuff').hide();
        @endif

        // $('#attach_photo').on('click', function(){
        //     $('#camera_stuff').slideDown();

        //     $('#camera_preview').show();
        //     $('#camera_result').hide();

        //     Webcam.attach( '#camera_preview' );
        // })

        $('#click_pic').on('click', take_snapshot);
  
  
    function take_snapshot() {
        Webcam.snap( function(data_uri) {
            $(".image-tag").val(data_uri);
            document.getElementById('camera_result').innerHTML = '<img src="'+data_uri+'"/>';

            Webcam.reset( '#camera_preview' );
            $('#camera_preview').hide();
            $('#camera_result').show();

        } );
    }
    });

</script>
@endsection