@extends('layouts.app')

@section('content')


    @if(Auth::user()->role == 'admin')

        u are admin


    @else
        u are not admin
    @endif



    <section>


        <legend>Today's Summary</legend>
        <div class="row text-center">

            <div class="col">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <h4 class="card-title">{{->count() }}
                            </h4>
                        <p class="card-text text-secondary">Gross Sale</p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <h4 class="card-title">{{\App\Trip::where('user_id', Auth::id())->whereNull('trip_sheet_id')->count() }}
                            </h4>
                        <p class="card-text text-secondary">Total Trips</p>
                    </div>
                </div>
            </div>
                


                
        </div>
        

    </section>
@endsection

