@extends('layouts.app')

@section('content')

            <div class="card border-primary">
                <div class="card-body">
                     <legend class="text-primary">Estimates</legend>

                   

               <div class="table-responsive">

                   <table style="margin-top:40px;" class="table table-hover table-sm">
                       <thead>
                           <tr>
                               <th style="width:80px;">ID</th>
                               <th>Created At</th>
                               <th style="width:180px;">Action</th>
                            </tr>
                            
                        </thead>
                        
                        <tbody>
                            @foreach($estimates as $row)
                            <tr>
                                <td>{{$row->id}}</td>
                            
                                <td>
                                    {{$row->created_at}} <span class="badge badge-pill badge-light"  data-toggle="tooltip" title="{{$row->created_at}}">{{$row->created_at->diffForHumans()}}</span>
                                    @if($row->creator)by <span class="badge badge-pill badge-light">{{$row->creator->name}}</span>@endif
                                </td>
                                <td>

                                    <a href="{{route('estimate.edit', $row->id)}}" class="btn btn-sm btn-outline-warning">View</a>
                                <form class="form-delete" method="post" action="{{route('estimate.destroy', $row->id)}}">
                                        @csrf
                                        {{method_field('DELETE')}}
                                        <button type="submit" class="btn btn-sm btn-outline-danger btn-delete">Delete</button>
                                    </form>
                                    
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                    {{$estimates->links()}}
                </div>
                
            </div>
            
            @endsection
            
            @section('scripts')
            <script type="text/javascript">
$(function(){
    $('.preview').on('click', function(){
        // $('#preview-modal .modal-content').html('<preview-component url="{{url('api/estimate')}}/'+$(this).data('id')+'"></preview-component>');
        $('#preview-modal .modal-content').find('preview-component').attr('url', "{{url('api/estimate')}}/"+$(this).data('id'))
        $('#printable').data('name', $(this).data('name'));
        $('#preview-modal').modal();
    });
    
    
    
    $('#export-pdf').on('click', function(){
        var doc = new jsPDF();
         doc.fromHTML($('#printable').html(), 15, 15, {
            'width': 170
            // 'elementHandlers': specialElementHandlers
            });
            // doc.setFont('Arial');
            var name = $('#printable').data('name');
        doc.save(name+'.pdf');
        doc = null;
    });


    $('#export-docx').on('click', function(){
        var converted = htmlDocx.asBlob($('#printable').html());
            var name = $('#printable').data('name');
        saveAs(converted, name+'.docx');
    });
});
</script>
@endsection