@extends('layouts.app')

@section('content')



 <form  action="{{route('estimate.update', $id)}}" method="POST" role="form">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PUT">

    <section id="app" class="calc-table">


        <div class="container-fluid">
            
            <div class="text-center">
            <h2 class="text-warning">Saved Deal # {{$id}}</h2>
            </div>
           
            <deal-calculator show_download_button="true" url="{{url('api/estimate/'.$id)}}"></deal-calculator>
            
           
            
        </div>
        
        
        <nav class="navbar fixed-bottom navbar-light bg-light"> 
            <a href="{{url('/estimate')}}" class="btn btn-secondary btn-outline-secondary float-right">Cancel</a>

            @if (session('status'))
                <div class="alert alert-success mb-0">
                    {{ session('status') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger mb-0">
                    {{$errors->first()}}
                </div>
            @endif
            <button class="btn btn-warning" type="submit">Update</button>
  
        </nav>

    </section>
     </form>


@endsection

@section('scripts')
@endsection