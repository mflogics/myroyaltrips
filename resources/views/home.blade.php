@extends('layouts.app') 
@section('content') @if(Auth::user()->role == 'super_admin' ||  Auth::user()->role == 'admin')

@if(Auth::user()->role == 'super_admin' || Auth::user()->role == 'admin')

<legend>Tripsheet Summary</legend>

<div class="row text-center">
    <div class="col-md-3 col-sm-6 mb-2">
        <div class="card shadow-sm">
            <a href="{{ url('tripsheets/submitted') }}">
                <div class="card-body">
                    <h4 class="card-title">{{$stats['NEW_TRIPSHEETS']}}
                    </h4>
                    <p class="card-text text-secondary">New Tripsheets</p>
                    <a href = "{{ url('download_csv/submitted') }}"><input type="button" class = "btn btn-success" value = "Download CSV"></input></a>
                </div>
            </a>

        </div>
    </div>

    <div class="col-md-3 col-sm-6 mb-2">
        <div class="card shadow-sm">
            <a href="{{ url('tripsheets/approved') }}">
                <div class="card-body">
                    <h4 class="card-title">{{$stats['APPROVED']}}
                    </h4>
                    <p class="card-text text-secondary">Approved Tripsheets</p>
                    <a href = "{{ url('download_csv/approved') }}"><input type="button" class = "btn btn-success" value = "Download CSV"></input></a>
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 mb-2">
        <div class="card shadow-sm">
            <a href="{{ url('tripsheets/rejected') }}">
                <div class="card-body">
                    <h4 class="card-title">{{$stats['REJECTED']}}
                    </h4>
                    <p class="card-text text-secondary">Rejected Tripsheets</p>
                    <a href = "{{ url('download_csv/rejected') }}"><input type="button" class = "btn btn-success" value = "Download CSV"></input></a>
                </div>
            </a>
        </div>
    </div>
    
    @if(Auth::user()->role == 'super_admin')
    <div class="col-md-3 col-sm-6 mb-2">
        <div class="card shadow-sm">
            <a href="{{ url('tripsheets/deleted') }}">
                <div class="card-body">
                    <h4 class="card-title">{{$stats['DELETED']}}
                    </h4>
                    <p class="card-text text-secondary">Deleted Tripsheets</p>
                    <a href = "{{ url('download_csv/deleted') }}"><input type="button" class = "btn btn-success" value = "Download CSV"></input></a>
                </div>
            </a>
        </div>
    </div>
 @endif
 
    <div class="col-md-3 col-sm-6 mb-2">
        <div class="card shadow-sm">
            <a href="{{ url('tripsheets/all') }}">
            <div class="card-body">
                <h4 class="card-title">{{$stats['TOTAL_TRIPSHEETS']}}
                </h4>
                <p class="card-text text-secondary">Total Tripsheets</p>
                
                <a href = "{{ url('download_csv/all') }}"><input type="button" class = "btn btn-success" value = "Download CSV"></input></a>
            </div>
        </div>
    </div>
</div>

@endif

<legend>Recent Tripsheets</legend>

<div class="table-responsive">

    <table class="table table-sm">
        <tr>
        <th>Driver</th>
        <th>Trip Sheet Date</th>
        <th>Status</th>
        <th>Note</th>
    </tr>
    @foreach ($recent_tripsheets as $ts)
    <tr>
        <td>{{$ts->user->name}}</td>
        <td>{{$ts->reservation_date}}</td>
        <td>{{$ts->status}}</td>
        <td>{{$ts->note}}</td>
    </tr>
    @endforeach
</table>
</div>

<legend>Balance of Submitted Tripsheets</legend>

<div class="table-responsive">

<table class="table table-sm">
    <tr>
        <th>Driver</th>
        <th>Car</th>
        <th>Driver Owes</th>
        <th>Status</th>
        <th>Trip Sheet Date</th>
    </tr>
    @foreach ($submitted_tripsheets as $ts)
    <tr>
        <td>{{$ts->user->name}}</td>
        <td>{{$ts->car}}</td>
        <td>${{$ts->getStats()['DRIVER_OWES']}}</td>
        <td>{{$ts->status}}</td>
        <td>{{$ts->reservation_date}}</td>
    </tr>
    @endforeach
</table>
</div>


{{-- <legend>2 Week Submission Overview</legend> --}}




@else

<legend>Current Tripsheet Summary</legend>

<div class="row text-center">
    <div class="col-md-3 col-sm-6 mb-2">
        <div class="card shadow-sm">
            <div class="card-body">
                <h4 class="card-title">{{$stats['TOTAL_TRIPS']}}
                </h4>
                <p class="card-text text-secondary">Total Trips</p>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6 mb-2">
        <div class="card shadow-sm">
            <div class="card-body">
                <h4 class="card-title">$ {{$stats['GROSS_SALE']}}</h4>
                <p class="card-text text-secondary">Gross Sale</p>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 mb-2">
        <div class="card shadow-sm">
            <div class="card-body">
                <h4 class="card-title">$ {{$stats['TOTAL_FEES']}}</h4>
                <p class="card-text text-secondary">Total Fees</p>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 mb-2">
        <div class="card shadow-sm">
            <div class="card-body">
                <h4 class="card-title">$ {{$stats['TOTAL_CASH_CREDIT_FARE']}}</h4>
                <p class="card-text text-secondary">Total Cash &amp; Credit Fare</p>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 mb-2">
        <div class="card shadow-sm">
            <div class="card-body">
                <h4 class="card-title">$ {{$stats['GROSS_TO_ROYAL']}}</h4>
                <p class="card-text text-secondary">Gross to Royal</p>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 mb-2">
        <div class="card shadow-sm">
            <div class="card-body">
                <h4 class="card-title">$ {{$stats['TOTAL_CREDIT_FARE_TRIPS']}}</h4>
                <p class="card-text text-secondary">Total Credit Fares/Tips</p>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 mb-2">
        <div class="card shadow-sm">
            <div class="card-body">
                <h4 class="card-title">$ {{$stats['TOTAL_CREDIT_CHARGES']}}</h4>
                <p class="card-text text-secondary">Total Credit Charges & Vouchers + Gas & Misc. Expenses </p>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6 mb-2">
        <div class="card border-warning text-warning  shadow-sm">
            <div class="card-body">
                <h4 class="card-title">$ {{$stats['DRIVER_OWES']}}</h4>
                <p class="card-text text-secondary">Driver Owes</p>
            </div>
        </div>
    </div>

</div>

<legend>My Recent Tripsheets</legend>
<div class="table-responsive">

<table class="table">
    <tr>
        <th>Trip Sheet Date</th>
        <th>Status</th>
        <th>Note</th>
    </tr>
    @foreach ($recent_tripsheets as $ts)
    <tr>
        <td>{{$ts->reservation_date}}</td>
        <td>{{$ts->status}}</td>
        <td>{{$ts->note}}</td>
    </tr>
    @endforeach
</table>
</div>


@endif
@endsection