<?php

use Illuminate\Http\Request;
use App\Estimate;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Estimate routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/project_types', function(){
    return ['test', 'test2'];
    return DB::table('projects')->select('type')->distinct()->get();
});


Route::get('trip_image/{id}', function($id){
    return \App\Trip::findOrFail($id)->image;
});


Route::post('estimate', function(){
    return ['test'];
});

Route::get('estimate/{id}', function($id){
    return  Estimate::findOrFail($id);
});