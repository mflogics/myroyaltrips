<?php
use Illuminate\Support\Facades\Notification;
use App\Notifications\TripsheetSubmitted;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','TripController@home');

Route::get('/test', function(){
    // if(env('NOTIFICATIONS_ENABLED') == true){
        $tripsheet = new \App\TripSheet();
        $user = new \App\User();
        $user->email = 'murizwan11@gmail.com';
        Notification::send($user, new TripsheetSubmitted($tripsheet));
    // }
});

Auth::routes();

Route::get('/home', 'TripController@home')->name('home');
Route::post('save_calculations', 'EstimateController@store')->name('save');
Route::post('submit_tripsheet', 'TripController@submitTripsheet');
Route::post('update_tripsheet/{id}', 'TripController@updateTripsheet');

Route::resource('estimate', 'EstimateController');
Route::resource('trip', 'TripController');
Route::get('tripsheets/{filter?}', 'TripController@history')->name('tripsheet.history');

Route::get('download_csv/{filter?}', 'TripController@download_csv_file');


Route::get('tripsheet/{id}/status/{status}', 'TripController@setTripsheetStatus');

Route::post('tripsheet/status/update', 'TripController@updateTripsheetStatus')->name('update.tripsheet');
Route::post('tripsheet/status/bulk_update', 'TripController@updateTripsheetBulkStatus')->name('bulk.update.tripsheet');
Route::post('tripsheet/payment/update', 'TripController@updateTripsheetPaymentStatus')->name('update.tripsheet.payment');
Route::post('tripsheet/uploadcvs', 'TripController@uploadCVSFile')->name('tripsheet.upload');
Route::post('tripsheet/comparecvs', 'TripController@CompareCVSFile')->name('tripsheet.compare');
Route::post('tripsheet/adminnoteupdate', 'TripController@adminNoteUpdate')->name('tripsheet.adminnoteupdate');
Route::get('tripsheet/adjustdatabase', 'TripController@adjustDatabase')->name('tripsheet.adjust');
//Route::get('tripsheet/{filter}', 'TripController@history');
Route::delete('tripsheet/{id}', 'TripController@destroyTripsheet');


Route::resource('user', 'UserController');

Route::get('settings', 'UserController@getSettings');
Route::post('settings', 'UserController@postSettings');

Route::get('locale/{locale}', 'UserController@setLocale');

// Route::get('monthly_driver_summary', function(){

// })


